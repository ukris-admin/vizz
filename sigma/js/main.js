$(document).ready(function () {
    console.log ("ready");
    
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-57482444-1', 'auto');
        ga('send', 'pageview');
    
    var networks = {
        "Lobistični stiki" : {data: "stikidata", text: "Omrežje vseh stikov med podjetji, njihovimi lobisti, ter politiki in državnimi institucijami, za katere ti delajo."},
        "Transakcije" : {data: "transakcijedata", text: "Omrežje transakcij med podjetji, v katerih so lobisti zakoniti zastopniki"},
        "Lobisti in podjetja" : {data: "firmedata", text: "Omrežje transakcij med podjetji, v katerih so lobisti zakoniti zastopniki"}
    };
    var currentNetwork = "stikidata"
    var tags = [];
    var margin = {top: 10, right: 20, bottom: 10, left:20};
    var tagsMap = {};
    var s;
	var theme = "integrity", s,
        parseDate = d3.time.format("%d-%m-%Y").parse,
		colors = {
			"lobist" : "#B8AB09",
			"lobirana organizacija" : "#790C2E",
			"organizacija" : "#1E7964",
			"lobiranec" : "#1294B8"
		},
		chart,chartWidth,chartHeight,
		basePoint = 12, multPoint = 1.5;
		;		
		
    var timeScale; 
    var yScale;
    var yearCheck = {};

    var slider = $("#slider").slider({
            max : 60,
            min : 0,
            step : 1,
            change : function(event, ui) {
                filterDegree = $('#slider').slider("value");
                filterGraphByDegree(filterDegree);
            
            },
            orientation : "horizontal"
            
     });

    $("#lightbox").modal("show");
    
	initChart();
	initSigma();
	initUI();
	addGraph1(currentNetwork);


    function initUI() {
        var nav = d3.select("#navigationBar");
        for (var sq in networks) {
            var square = nav.append("div").attr("class", "navSquare").attr("data-nav", networks[sq].data).attr("id", networks[sq].data);
            square.append ("p").style("font-size", "1.3em").style("font-weight", "bold").text(sq);
            square.append ("p").attr("class", "navText").text(networks[sq].text);
            square.on ("click", function (e) {
                $(".navSquare").css({"background-color" : "#f1f1f1", color: "black"});
                $(this).css({"background-color" : "#00546c", color: "white"});
                currentNetwork =  ($(this).attr("data-nav"));
                addGraph1(currentNetwork);
                ga('send', 'event', 'Navigation', currentNetwork, 'lobistični stiki', 1);
                switch (currentNetwork) {
                    case "stikidata":
                        $("#yearcontrol").show();
                        break;
                    default:
                        $("#yearcontrol").hide();
                        $("#chart").hide();
                        break;
                }
            });
        }
        $("#stikidata").css({"background-color" : "#00546c", color: "white"});
        $("#info").click(function () {
            switch(currentNetwork) {
                case "stikidata":
                    $("#modalStiki").modal("show");
                    break;
                case "transakcijedata":
                    $("#modalTransakcije").modal("show");
                    break;
                case "firmedata":
                    $("#modalFirme").modal("show");
                    break;
            }

        });
    }
	
    function getColor (type) {
       return colors[type]; 
    }

   function addGraph1(url, options) {
       tags = [];
       tagsMap = {};
        $.ajax({
            url : "./data.json",
            async : true
        })
        .done(function(json) {
            s.graph.clear();

            var edges = json.edges;
            var nodes = json.nodes;
            var type, label;
            $.each(nodes, function(i, value) {
                //console.log (value.attributes);
                type = value.attributes.Type;
                label = (type == "lobist") ? "" : value.label;
                var dates = value.attributes.Dates;
                if (dates != undefined) dates = $.parseJSON(dates);
                
                s.graph.addNode({
                    id : value.id,
                    label : label,
                    x : value.x,
                    y : value.y,
                    color : getColor (value.attributes.Type),//value.color,
                    originalColor : getColor (value.attributes.Type),
                    size : value.size,
                    type : type,
                    purposes : value.attributes.Purposes,
                    dates : dates,//$.parseJSON(value.attributes.Dates),
                    locations: value.attributes.Locations,
                    people: value.attributes.People,
                    organizations: value.attributes.Organizations
                });
                if (type != "lobist") tags.push(value.label);
                tagsMap[value.label] = {type: value.attributes.Type, node: s.graph.nodes(value.id)};
                
            });
            
            var edgeID = 0;
            $.each(edges, function(index, value) {
                //console.log(value);
                s.graph.addEdge({
                    id : "id" + edgeID,//value.id,
                    source : value.source,
                    target : value.target,
                    size : value.size,
                    color:"#ccc",
                    originalColor:"#ccc",
                    label: value.label
                });
                edgeID++;
            });
           filterGraphByDegree(1);
           s.refresh();
            //console.log (tags);
            $( "#searchField" ).autocomplete({
                source: tags,
                select: function (event, ui) {
                    var name = ui.item.value;
                    var node = tagsMap[name].node;
                    s.cameras[0].goTo({x:node['read_cam0:x'],y:node['read_cam0:y'],ratio:0.1/node.size});
                    if (currentNetwork == "stikidata") shunt (tagsMap[name].node);
                }
            });
        });

    }
	
	function getEdgeColor (type) {

    }


	function initChart() {
		chartWidth = parseInt(d3.select('#barchart').style('width'));	  
		chartHeight = parseInt(d3.select('#barchart').style('height'));; 	
		chart = d3.select("#barchart").append("svg")
	     .attr("height", (200 - margin.top - margin.bottom) + "px")
	     .attr("width", (chartWidth - margin.left - margin.right) + "px");
	     ;
	}
	

	function initSigma() {
        //s = new sigma();
        //var cam = s.addCamera();
		s = new sigma(
			{
			  container: document.getElementById('graph'),
			  type: 'webgl',
			  //camera: cam,
              settings: {
                defaultLabelColor: '#000',
                labelSize : 'proportional',
                labelSizeRatio : 1.5,
                labelThreshold : 5,
                drawEdges : true,
                minNodeSize : 1,
                maxNodeSize : 15,
                minEdgeSize : .2,
                maxEdgeSize : 5.0,
                zoomMin : 0.01,
                zoomMax : 4.9,
                font : "Rajdhani"
              }
			}			
		);
		
		//s.startForceAtlas2();


        s.bind('clickNode', function(e) {
            var clickednode = e.data.node;
            var connected = [];
            var lobbyists = {};
            var organizations = {};
            $.each(s.graph.edges(), function (i, d) {
                if (d.source == clickednode.id || d.target == clickednode.id) {
                    if (currentNetwork == "stikidata") shunt (clickednode);
                    return;
                }
            });
            //console.log (clickednode);
        });

        s.bind("overNode", function (e) {
            var nodeId = e.data.node.id,
                toKeep = s.graph.neighbors(nodeId);

            s.graph.nodes().forEach(function(n) {
                if (toKeep[n.id])
                    n.color = n.originalColor;
                else n.color = '#f1f1f1';
                //n.hidden = true;
            });

            s.graph.edges().forEach(function(e) {
                if (toKeep[e.source] && toKeep[e.target])
                    e.color = '#666'//e.originalColor;
                else e.color = '#f1f1f1';
            });
            s.refresh();
        });

        s.bind('outNode', function(e) {
            s.graph.nodes().forEach(function(n) {
                n.color = n.originalColor;
                //n.hidden = false;
            });

            s.graph.edges().forEach(function(e) {
                e.color = e.originalColor;
            });
           
            // Same as in the previous event:
            s.refresh();
        });


	}
	
	$("#closePane").click (function () {
	   $("#chart").hide(); 
	});
	
	function shunt (node) {
	    switch (node.type) {
	        case "lobiranec" :
	           drawPolitician (node.people, node.organizations, node.dates, node.purposes, node.locations, node.label, "lobiranec");
	           break;
	        case "lobist" :
	           drawPolitician (node.people, node.organizations, node.dates, node.purposes, node.locations, node.label, "lobist");
	           break;
	        case "lobirana organizacija" :
	           drawPolitician (node.people, node.organizations, node.dates, node.purposes, node.locations, node.label, "institucija");
                break;
            case "organizacija" :
               drawPolitician (node.people, node.organizations, node.dates, node.purposes, node.locations, node.label, "organizacija");	           
	        default:
	           break;
	    }

	}
	
	function drawPolitician (people, organizations, dates, purposes, locations, name, type) {
	    ga('send', 'event', 'Details', name, type, 1);
        $("#loader").show();
	    $("#chart").show();
	    $("#people").empty();
	    $("#organizations").empty();
	    $("#title").empty();
	    $("#locations").empty();
	    $("#purposes").empty();
	    var peopleDiv = d3.select ("#people");
	    var orgDiv = d3.select ("#organizations");
	    var titleDiv = d3.select ("#title");
	    var purpDiv = d3.select ("#purposes");
	    var locDiv = d3.select ("#locations");
	    var dateArray = [];
	    people = JSON.parse(people);
	    organizations = JSON.parse(organizations);
	    locations = JSON.parse(locations);
	    purposes = JSON.parse(purposes);
	    //dates = JSON.parse(dates);
        if (type != "lobist") {
            titleDiv.text(name + ", " + type);
        }
        else titleDiv.text(type);


	    orgDiv.append("div").attr("class", "littleTitle").text("ORGANIZACIJE");
	    purpDiv.append("p").attr("class", "littleTitle").text("NAMENI LOBIRANJA");
	    locDiv.append("p").attr("class", "littleTitle").text("KRAJI LOBIRANJA");
	    
	    if (type == "lobist" || type == "organizacija") {
	        var peopleLabel = (type == "lobiranec" || type == "institucija") ? "LOBISTI" : "LOBIRANCI";
            peopleDiv.append("p").attr("class", "littleTitle").text(peopleLabel);
            var peopleArray = objToArray(people);
            outputArray (peopleArray, peopleDiv, true);
	    }
        else {
            peopleDiv.append("p").text("Seznama lobistov ne moremo prikazati zaradi zahteve informacijske pooblaščenke po varstvu osebnih podatkov.");
        }

        
        var orgArray = objToArray(organizations);
        outputArray (orgArray, orgDiv, true);
        
        var locArray = objToArray(locations);
        outputArray (locArray, locDiv, false);
	    
        var purpArray = objToArray(purposes);
        outputArray (purpArray, purpDiv, false);
    
        
        for (var da in dates) {
            dateArray.push({date: parseDate(da), val: dates[da]});
        }  
        
        dateArray.sort (function (a, b) {
            return a.date - b.date;
        });
        
	    chartWidth = parseInt(d3.select('#barchart').style('width')) * 0.95 ;    
	    timeScale = d3.time.scale().range([0, chartWidth]).domain([dateArray[0].date, new Date()]);
	    yScale = d3.scale.linear().range([0, chartHeight]).domain([0, d3.max(dateArray, function (d) {return d.val;})]);
	    var barWidth = chartWidth / dateArray.length;
	    chart.attr("width", chartWidth + "px");
	    chart.selectAll ("rect").remove();
        chart.selectAll ("g").remove();

        var date1, date2;
        if (dateArray.length > 1) {
            date1 = dateArray[0].date;
            date2 = dateArray[dateArray.length-1].date;
        }
        else if (dateArray.length == 1) {
            date1 = dateArray[0].date;
            date2 = dateArray[0].date;
        }
        var timeDiff = Math.abs(date1.getTime() - date2.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        //console.log (diffDays);
        var barwidth = chartWidth/diffDays;
        if (diffDays == 0) barwidth = 10;
        if (barwidth < 2) barwidth = 2;

        var xAxis = d3.svg.axis()
            .scale(timeScale)
            .orient('bottom')
            .ticks(5)
            .tickFormat(d3.time.format('%m / %Y'))
            .tickSize(3)
            .tickPadding(4);

        chart.selectAll("rect").data(dateArray).enter()
	       .append("rect")
	       .attr ("width", barwidth)
	       .attr ("height", function (d) {return yScale(d.val);})
	       .attr ("x", function (i, d) {return timeScale(i.date);})
	       .attr ("y", function (d) {return 200 - yScale(d.val) - 40;})
	       .style ("fill", "#e92461")
	       //.style("stroke", "white")
	    
	    ;

        chart.append ("g")
            .attr('class', 'x axis')
            .attr('transform', 'translate(0, 160)')
            .call(xAxis);
        $("#loader").hide();
	}
	
	function outputArray (array, div, click) {
        var fontmax = d3.max (array, function (d) {return d.val;});
	    array.forEach (function (d) {
            //var node = (tagsMap[d.key].node);
            var innn = div.append ("p")
               .attr ("class", (click == true) ? "textIn" : "textIn1")
               .style ("font-size", (basePoint + 10 *  d.val/fontmax) + "px") //V
               .text(d.key)
                /*
               .on ("click", function () {
                    s.cameras[0].goTo({x:node['read_cam0:x'],y:node['read_cam0:y'],ratio:0.1/node.size});
                });
                */
            ;
            if (click) {
                var node = (tagsMap[d.key].node);
                innn.on ("click", function () {
                    s.cameras[0].goTo({x:node['read_cam0:x'],y:node['read_cam0:y'],ratio:0.1/node.size});
                });
            }
	    });
	}
	
	function objToArray (obj) {
	    var array = [];
	    for (var o in obj) {
	        array.push({key: o, val: obj[o]});
	    }
	    array.sort (function (a, b) {
	        if (a.val > b.val) return -1;
	        else if (a.val < b.val) return 1;
	        else return 0;
	    });
	    return array;
	}
	

	
	function getColor2 (type, mode) {
		var result = "rgb(102,153,255)";
		if (colors[type])  result = (mode == "color") ? colors[type][0] : colors[type][1];
		return result;
	}
	
	
	   function getColor3 (type, mode) {
        var result = "rgb(102,153,255)";
        if (colors[type])  result = colors[type][0];
        return result;
    }
    

	
	function filterGraphByDegree(degree) {
        //console.log("filter by: " + degree);
        ga('send', 'event', 'Filters', "degree", degree, 1);
        $.each(s.graph.nodes(), function(i, v) {
            //console.log (v.label + " exists: " + existsInYear(v, "2012"));
            if (s.graph.degree(v.id) < degree)
                v.hidden = true;
            else
                v.hidden = false;
        });
        
        s.startForceAtlas2();
        s.stopForceAtlas2();
        s.refresh;
        
    }
    
    $("input.yr").change (function (e) {
        var year =  $(this).attr("data-nav");
        ga('send', 'event', 'Filters', "year", year, 1);
        if (yearCheck[year] == undefined) yearCheck[year] = true;
        else delete yearCheck[year];

       filterGraphByYear (yearCheck);
    });
    
    function filterGraphByYear(yearCheck) {
        
        var hide = 0;
        var nrk = $.map(yearCheck, function(n, i) { return i; }).length;//Object.keys(yearCheck).length;
        
        $.each(s.graph.nodes(), function(i, v) {
                if (nrk == 0) {
                    v.hidden = false;
                }
                else {
                    for (var year in yearCheck) {
                        
                        if (existsInYear (v, year)) {
                            v.hidden = false;
                            break;
                        }
                        else {
                            v.hidden = true;
                            hide++;
                            
                        }
                    }
                }
        });
        
        s.startForceAtlas2();
        s.stopForceAtlas2();
        s.refresh;        
    }    
    
    function existsInYear (node, year) {
        var dates = node.dates;
        
        for (var date in dates) {
            if (date.indexOf(year) > 0) {
                return true;
            }
        }
        return false;
    }
    

    Object.prototype.hasOwnProperty = function(property) {
        return this[property] !== undefined;
    };


});
