// var graph;
// TODO:
//  1) Add Legend
//  2) Pan/Zoom - center Node
//  3) Fix search reset bug
//  4) Show Path
//  5) Additional Tooltip
//  6) Code Refactor
//     - 2 places where ajax is called
//     - array to tree converge

(function(angular, app, undefined) {
    app.controller("TopologyController", ["$scope", "$rootScope", "$window", "$timeout", "DeviceService", "$http", "$route", "$timeout", "$location", "CheckedDataTypes",
        function($scope, $rootScope, $window, $timeout, DeviceService, $http, $route, timeout, $location, CheckedDataTypes) {
            $scope.lock = true;
            $scope.toggleLock = function() {
                //$scope.lock = !$scope.lock;
                if ($scope.lock) {
                    // delelect the current device
                    $scope.currentDevice = null;

                    $scope.selectedDevices = [];
                    angular.forEach($scope.devices, function(val, key) {
                        val.selected = false;
                    });
                }
            };

            $scope.unlinked = [];
            $scope.topology = { parent: [], children: [] };

            $scope.currentTime;
            $scope.selectedDevices = [];
            $scope.currentDevice = null;
            $scope.summaryDevice = null;
            $scope.didDrag = false;
            $scope.mapRefresh = false;
            var expectedDevices = "G2";

            $scope.editParentChild = function() {
                if (!$scope.currentDevice) return false;

                var currDev = $scope.currentDevice;
                $scope.topology = { parent: [], children: [] };

                if (currDev.parents || currDev.children) {
                    angular.forEach(currDev.parents, function(id) {
                        if ($scope.devices[id] && $scope.devices[id].modelName != expectedDevices)
                            $scope.topology.parent.push($scope.devices[id]);
                    });

                    angular.forEach(currDev.children, function(id) {
                        if ($scope.devices[id] && $scope.devices[id].modelName != expectedDevices)
                            $scope.topology.children.push($scope.devices[id]);
                    });
                };

                $scope.unlinked = [];
                $scope.topoDevice = null;
                angular.forEach($scope.devices, function(d) {
                    if ($scope.deviceFilter(d) && d.modelName != expectedDevices) {
                        $scope.unlinked.push(d);
                    }
                });
            };
            $scope.changeNetworkBulk = function(org) {
                var divArr = [];
                angular.forEach($scope.selectedDevices, function(d) {
                    if (d.selected) divArr.push(d.id);
                });
                //Devices change network
                $http.put('/organization/' + org.id + '/move/devices?devices=' + divArr.join(",")).success(function(data, status) {
                    if (typeof data === "string") {
                        $scope.alert(data.replace("errorMessage:", ""));
                    } else {
                        timeout(function() {
                            $scope.alert("Device(s) moved succesfully.");
                            $route.reload();
                        }, 300);
                    }
                });
            };
            if (!Array.prototype.uniq) {
                Array.prototype.uniq = function(a) {
                    var a = this;
                    a.sort();
                    var i,j;
                    for (i=0;i<a.length;i++) {
                        j= a.indexOf(a[i]);
                        if (i !== j) {
                            a.splice(j,1);
                            i--;
                        }
                    }
                }
            }

            $scope.showChangeNetDialog = function() {
                $scope.selectedNet = null;

                angular.forEach($scope.selectedDevices, function(d) {
                    d.selected = true;
                });

                if ($scope.selectedDevices.length === 0) {
                    $scope.alert("Select device for network change.");
                    return;
                }
            };

            $scope.checkSelectedDeviceList = function() {
                var flag = false;
                angular.forEach($scope.selectedDevices, function(d) {
                    if (d.selected) flag = true;
                });
                return flag;
            };

            $scope.changeNetwork = function(org) {
                $http.put('/device/' + $scope.currentDevice.id + '/organization/' + org.id).success(function(data, status) {
                    if (typeof data === "string") {
                        $scope.alert(data.replace("errorMessage:", ""));
                    } else {
                        timeout(function() {
                            $scope.alert("Device moved succesfully.");
                            $route.reload();
                        }, 300);

                    }
                });
            };

            $scope.$on('currentDeviceNameEvnt', function(event, data) {
                $scope.deviceName = data.friendlyName;
                $scope.selectedNet = null;
            });

            $http.get("/organization").success(function(resp) {
                $scope.selectedNet = null;
                $scope.networks = resp;
            });

            $scope.assingNet = function(network) {
                $scope.selectedNet = network;
            };

            $scope.deviceFilter = function(d) {
                currDevice = $scope.currentDevice;

                for (var i = 0; i < currDevice.parents.length; i++) {
                    if (d.id === currDevice.parents[i]) {
                        return false;
                    }
                }

                for (var i = 0; i < currDevice.children.length; i++) {
                    if (d.id === currDevice.children[i]) {
                        return false;
                    }
                }

                if (currDevice.id == d.id) return false;

                return true;
            };

            $scope.addParentOrChild = function(b, dev) {
                $scope.topoDevice = dev;
                if (b) {
                    $scope.topology.parent.push(dev);
                } else {
                    $scope.topology.children.push(dev);
                }
                $scope.unlinked.splice($scope.unlinked.indexOf(dev), 1);
            };

            $scope.removeParentOrChild = function(b, dev) {
                if (b) {
                    $scope.topology.parent.splice($scope.topology.parent.indexOf(dev), 1);
                } else {
                    $scope.topology.children.splice($scope.topology.children.indexOf(dev), 1);
                }
                $scope.topoDevice = null;
                $scope.unlinked.push($scope.devices[dev.id]);
            };

            $scope.updateDevice = function() {
                DeviceService.getDevice($scope.currentUser.defaultOrg.id, $scope.currentDevice.id, function(dbDevice) {
                    dbDevice.friendlyName = $scope.deviceName || dbDevice.friendlyName;
                    DeviceService.editDeviceSummary($scope.currentUser.defaultOrg.id, dbDevice, function(data) {
                        $scope.alert("Device updated");
                        $window.location.reload();
                    });
                });
            };

            $scope.updateDeviceTopology = function() {
                DeviceService.getDevice($scope.currentUser.defaultOrg.id, $scope.currentDevice.id, function(dbDevice) {
                    //                console.log(dbDevice);
                    dbDevice.parents = {};
                    dbDevice.children = {};

                    //TODO: other updated values goes here
                    angular.forEach($scope.topology.parent, function(item) {
                        dbDevice.parents[item.id] = item.friendlyName;
                    });

                    angular.forEach($scope.topology.children, function(item) {
                        dbDevice.children[item.id] = item.friendlyName;
                    });


                    DeviceService.editDeviceSummary($scope.currentUser.defaultOrg.id, dbDevice, function(data) {
                        $scope.alert("Device updated");
                        $window.location.reload();
                    });
                });
            };

            $scope.toggleSelected = function(d) {
                d.selected = !d.selected;
                var idx = $scope.selectedDevices.indexOf(d);
                if (d.selected && idx === -1) {
                    $scope.selectedDevices.push(d);
                } else {
                    $scope.selectedDevices.splice(idx, 1);
                }
                // console.log($scope.selectedDevices);
            };
            var orgId = 1,
                flatTreeArray,
                jsonTree = null,
                origJsonTree = null,
                width = $(window).width() - 100,
                height = $(window).height() - 100,
                $treeContent,
                treeContent = '#tree-content',
                shape,
                newSearch,
                initSearch,
                prevFilter,
                prevShowPath,
                showPath,
                crit;
            
            $scope.showTree = function() {
                var searchInput = '',
                    Links,
                    Nodes,
                    Many,
                    rootName = 'root',
                    filterArray,
                    DEFAULT = 'DEFAULT',
                    NOT_MONITORED = 'NOT_MONITORED',
                    OK = 'OK',
                    FAILING = 'FAILING',
                    sizes = {
                        'A5C': 21,
                        'A5': 18,
                        'B': 13,
                        'B5C': 15,
                        'B5': 12,
                        'C5C': 9,
                        'C5': 6,
                        'G2': 3,
                        DEFAULT: 3
                    },
                    centerX = 250,
                    centerY = 250,
                    node_icons = {

                    },
                    status_colors = {
                        FAILING: '#d01717',
                        NOT_MONITORED: '#808080',
                        OK: '#8bc540',
                        DEFAULT: '#0000FF'
                    },
                    filterArray,
                    deviceTypes,
                    status,
                    SHAPE_SIZE = 200,
                    symbol,
                    shape,
                    inFilter,
                    line_colors = {
                        'A': '#FF000',
                        'A5C': '#FF000',
                        'A5': '#E76E3C',
                        'B': '#800080',
                        'B5C': '#800080',
                        'B5': '#58007E',
                        'C': '#008000',
                        'C5C': '#008000',
                        'C5': '#205E3B',
                        'G': '#2B2B2B',
                        'G2': '#2B2B2B',
                        DEFAULT: '#000'
                    },
                    MAX_TEXT_SIZE = 10,
                    defaultStyle = {
                        padding: "0px 5px 0px 5px",
                        margin: "5px",
                        "border-radius": "16px",
                        "background-color": "white",
                        "stroke": "none",
                        "cursor": "pointer"
                    },
                    hoveredStyle = {
                        "background-color": "lightgray"
                    },
                    clickedStyle = {
                        "background-color": "gray"
                    },

                    hoveredStyle = { "background-color": "lightgray" },
                    clickedStyle = { "background-color": "gray" },
                    margin = { top: 40, right: 20, bottom: 20, left: 20 },

                    searchFound = function(item) {
                        if (!crit) {
                            crit = $scope.search.crit;
                        }
                        if ((!crit) || (!crit.length)) {
                            return true;
                        }
                        var nm = item.name.trim().toUpperCase();
                        ty = item.type;
                        st = item.status,
                            res = false;
                        for (i = 0; i < crit.length; i++) {
                            switch (crit[i].type) {
                                case 'name':
                                    if (~nm.indexOf(crit[i].val)) {
                                        res = true;
                                    }
                                    break;
                                case 'type':
                                    if (~ty.indexOf(crit[i].val)) {
                                        res = true;
                                    }
                                    break;
                                case 'status':
                                    if (~st.indexOf(crit[i].val)) {
                                        res = true;
                                    }
                            }
                            if (res) {
                                return true;
                            }
                        }
                        return false;
                    },
                    setAttr = function(node, o) {
                        node.name = o.name || o.friendlyName;
                        node.id = o.id;
                        if (newSearch) {
                            $scope.search.names.push(node.name);
                        }
                        // node.children = o.children.slice(0);
                        // node.parents = o.parents.slice(0);
                        node.type = o.modelName.trim().toUpperCase();
                        if (deviceTypes[node.type]) {
                            deviceTypes[node.type]++;
                        } else {
                            deviceTypes[node.type] = 1;
                            $scope.search.types.push(node.type);
                        }

                        if (line_colors[node.type]) {
                            node.line_color = line_colors[node.type];
                            node.size = sizes[node.type];
                        } else {
                            var a = node.type.substring(0, 2);
                            if (!line_colors[a]) {
                                a = node.type.substring(0, 1);
                            }
                            if (line_colors[a]) {
                                node.line_color = line_colors[a];
                                node.size = sizes[a];
                            } else {
                                node.line_color = line_colors[DEFAULT];
                                node.size = sizes[DEFAULT];
                            }
                        }
                        if (!node.color) {
                            if (!o.monitored) {
                                node.status = NOT_MONITORED;
                                if (status[NOT_MONITORED]) {
                                    status[NOT_MONITORED]++
                                } else {
                                    status[NOT_MONITORED] = 1;
                                    $scope.search.statuses.push(NOT_MONITORED);
                                }
                            } else if (o.severity === OK) {
                                node.status = OK;
                                if (status[OK]) {
                                    status[OK]++
                                } else {
                                    status[OK] = 1;
                                    $scope.search.statuses.push(OK);
                                }
                            } else {
                                node.status = FAILING;
                                if (status[FAILING]) {
                                    status[FAILING]++;
                                } else {
                                    status[FAILING] = 1;
                                    $scope.search.statuses.push(FAILING);
                                }
                            }
                        }
                    },
                    filterResult = function() {
                        var primaryNode = {
                                id: "root",
                                kids: [],
                                name: rootName,
                                type: 'R'
                            },
                            sourceNodes = {};

                        deviceTypes = {};
                        status = {};
                       
                        // get children for root;
                        flatTreeArray.forEach(function(node) {
                            var newNode = {};
                            setAttr(newNode, node);
                            newNode.kids = [].concat(node.children);

                            if (!node.parents || !node.parents.length) {
                                newNode.parents = [primaryNode.id];
                                primaryNode.kids.push(newNode.id);
                            } else {
                                newNode.parents = [].concat(node.parents);
                            }

                            if (!node.name) {
                                node.name = node.friendlyName;
                            }
                            // create diction for quick access;
                            sourceNodes[newNode.id] = newNode;
                            // clear flag;
                            newNode.hasParent = false;
                            node.found = false;
                        });

                        var findDown = function(node, found) {

                            if (searchFound(node)) {
                                node.found = true;
                                return true;
                            }

                            if (!found && node.kids && node.kids.length) {
                                node.kids.forEach(function(kidId) {
                                    var childNode = sourceNodes[kidId];
                                    if (childNode) {
                                        found = findDown(childNode, found);
                                    }
                                });
                            }
                            return found;
                        }

                        var loadChild = function(node, nonCheck) {
                            var children = [];

                            if (node.kids && node.kids.length) {
                                node.kids.forEach(function(kidId) {
                                    var childNode = sourceNodes[kidId];
                                    // First come, first served
                                    if (childNode && !childNode.hasParent) {
                                        childNode = Object.assign({}, childNode);
                                        sourceNodes[kidId].hasParent = true;
                                        if (nonCheck || node.found) {
                                            loadChild(childNode, true);
                                            children.push(childNode);
                                        } else {
                                            if (findDown(childNode, false)) {
                                                children.push(childNode);
                                            }
                                            loadChild(childNode, false);
                                        }
                                    }
                                });
                            }

                            if (node.found) {
                                node._children = children;
                            } else {
                                // re-assign children;
                                node.children = children;
                            }
                        }

                        if (crit && crit.length) {
                            loadChild(primaryNode, false);
                        } else {
                            loadChild(primaryNode, true);
                        }

                        jsonTree = primaryNode;
                        if (!origJsonTree) {
                            origJsonTree = JSON.parse(JSON.stringify(jsonTree));
                            $scope.search.types.uniq();
                            $scope.search.names.uniq();
                            $scope.search.statuses.uniq();
                        }
                        newSearch = false;
                    },
                   
                    showDND = function() {
                        // Calculate total nodes, max label length
                        var totalNodes = 0,
                            maxLabelLength = 0,
                            // variables for drag/drop
                            
                            // Misc. variables
                            i = 0,
                            duration = 750,
                            root,
                            domNode,
                            dragStarted,
                            nodes,
                            links,
                          
                            // size of the diagram
                            viewerWidth = $(window).width(),
                            viewerHeight = $(window).height(),
                            nodeWidth = 300,
                            nodeHeight = 75,
                            horizontalSeparationBetweenNodes = 16,
                            verticalSeparationBetweenNodes = 128,
                            $bubbleContainer = $('#nodeBubbleContainer'),
                            firstTime = true,
                            baseSvg,
                            svgWrapper,
                            maxX,
                            maxY,
                            overlay;
                           

                        if (!shape) {
                            var symbol = d3.svg.symbol().size(SHAPE_SIZE).type;
                            shape = {
                                'A': symbol('diamond')(),
                                'B': symbol('circle')(),
                                'C': symbol('triangle-up')(),
                                'G': symbol('square')(),
                                'R': symbol('cross')(),
                                DEFAULT: symbol('triangle-down')()
                            };
                        }
                        var tree = d3.layout.tree()
                            .nodeSize([nodeWidth + horizontalSeparationBetweenNodes, nodeHeight + verticalSeparationBetweenNodes])
                            .separation(function(a, b) {
                                return a.parent == b.parent ? 2 : 2.5;
                            });

                        // define a d3 diagonal projection for use by the node paths later on.
                        var diagonal = d3.svg.diagonal()
                            .projection(function(d) {
                                return [d.y, d.x];
                            });

                        // A recursive helper function for performing some setup by walking through all nodes
                        function visit(parent, visitFn, childrenFn) {
                            if (!parent) return;
                            visitFn(parent);
                            var children = childrenFn(parent);
                            if (children) {
                                var count = children.length;
                                for (var i = 0; i < count; i++) {
                                    visit(children[i], visitFn, childrenFn);
                                }
                            }
                        }

                        // Call visit function to establish maxLabelLength
                        visit(jsonTree, function(d) {
                            totalNodes++;
                            if (!d.name) {
                                d.name = d.friendlyName || "";
                            }
                            var len = Math.min(d.name.length, 20);
                            maxLabelLength = Math.max(len, maxLabelLength);
                        }, function(d) {
                            return d.children && d.children.length > 0 ? d.children : null;
                        });
                        // sort the tree according to the node names

                        function sortTree() {
                            tree.sort(function(a, b) {
                                return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
                            });
                        }
                        // Sort the tree initially incase the JSON isn't in a sorted order.
                        sortTree();

                        // Define the zoom function for the zoomable tree
                        function zoom(pos,scale) {
                            if (!pos) {
                                pos = d3.event.translate;
                                scale = d3.event.scale;
                            }
                            if (centered) {
                                centered = false;
                            }
                            else {
                                baseSvg
                                .attr("transform", "translate(" + pos + ")scale(" + scale + ")");
                            }
                        }

                        function expand(d){   
                            var children = (d.children)?d.children:d._children;
                            if (d._children) {        
                                d.children = d._children;
                                d._children = null;       
                            }
                            if(children)
                              children.forEach(expand);
                        }

                        function expandAll(){
                            expand(root); 
                            update(root);
                        }

                        function collapseAll(){
                            root.children.forEach(collapse);
                            collapse(root);
                            update(root);
                        }

                        // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
                        var zoomListener = d3.behavior.zoom().scaleExtent([0.5, 10]).on("zoom", zoom);

                        // define the baseSvg, attaching a class for styling and the zoomListener
                        $('#tree-content').find('svg').remove();
                        var legend = $('#tree-content').find('.legend')[0];
                        $(legend).html('');
                        legend = d3.select('#tree-content').select('.legend');
                        var symbolScale = d3.scale.ordinal()
                            .domain(['A', 'B', 'C', 'G', 'Root', 'Default'])
                            .range([shape.A, shape.B, shape.C, shape.G, shape.R, shape.DEFAULT]);

                        legend = legend.append('svg');

                        legend.append("g")
                            .attr("class", "legendSymbol")
                            .attr("transform", "translate(20, 20)");

                        var legendPath = d3.legend.symbol()
                            .scale(symbolScale)
                            .orient("vertical");

                        legend.select(".legendSymbol")
                            .call(legendPath);

                        svgWrapper = d3.select("#tree-content").append("svg")
                            .attr("class", "overlay")
                            .attr("height", "100%")
                            .attr("width", "100%")
                            .call(zoomListener);

                        baseSvg = svgWrapper.append("g") //.attr("transform", "translate("+centerX+","+centerY+")");
                            //zoomListener.translate([centerX,centerY]);
                            // Define the drag listeners for drag/drop behaviour of nodes.

                        // Toggle children function
                        // Append a group which holds all nodes and which the zoom Listener can act upon.
                       // var svgGroup = baseSvg.append("g").attr("class", "pan-cover");
                        var childGroup = baseSvg.append("g").attr("class", "child-group");
                        // Define the root
                        root = jsonTree;
                        root.x0 = viewerHeight / 2;
                        root.y0 = 0;
                        var isRotate = false;
                        var rotateButton = $("#rotate-button").click(function() {
                            isRotate = !!!isRotate;
                            if (isRotate) {
                                childGroup.attr("transform", "rotate(90)");
                            } else {
                                childGroup.attr("transform", "rotate(0)");
                            }
                            centerNode(root);
                            //update(root);
                        });
                        
                        function toggleChildren(d) {
                            if (d.children) {
                                d._children = d.children;
                                d.children = null;
                            } else  {
                                d.children = d._children;
                                d._children = null;
                            }
                            update(d);
                        }

                        // Toggle children on double click.
                        function dblClick(d) {
                            closeBubble();
                            if (d3.event.defaultPrevented) return; // click suppresseed
                            d = toggleChildren(d);
                            d3.event.stopPropagation()
                            d3.event.preventDefault();
                            
                        }

                        // show tooltip on click
                        function click(d) {
                            var node = _.find(flatTreeArray, { id: d.id });
                            $scope.currentDevice = node;
                            $scope.$apply();
                        }

                        function getExtraLinks(nodes) {
                            var connect = [],
                                nodeIds = {},
                                parents = [];

                            for (var i = 0; i < nodes.length; ++i) {
                                nodeIds[nodes[i].id] = nodes[i];
                            }

                            for (var i = 0; i < nodes.length; ++i) {
                                parents = nodes[i].parents;
                                if (parents && parents.length) {
                                    for (var p = 0; p < parents.length; ++p) {
                                        var parent = nodeIds[parents[p]];
                                        parent || (parent = nodeIds.root)
                                        connect.push({
                                            source: parent,
                                            target: nodes[i]
                                        });
                                    }
                                }
                            }
                            return connect;
                        }
                        var centered = false;
                        // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.
                        function centerNode() {
                            if (!overlay) {
                                overlay = $('#tree-content .overlay')
                            }
                            var scale = zoomListener.scale(),
                                x = $("#scroll-box").width(),
                                y = $("#scroll-box").height(),
                                scalex,scaley,posx,posy,
                                w = parseInt(overlay.attr('width'), 10);
                                h = parseInt(overlay.attr('height'), 10);
                            
                            scalex = Math.round(x / w * 100) / 100;
                            scaley = Math.round(y / h * 100) / 100;
                            scale = scalex < scaley ? scalex : scaley;
                            if (scale < 0.5) {
                                scale = 0.5;
                            }
                            posx = Math.round(w / 2 ); 
                            posy = Math.round(h / 2);
                            if (h > y) {
                                if (isRotate) {
                                    posx = Math.round(posy * scale) + viewerWidth/2;
                                    posy = 200;
                                }
                                else {
                                    posy = -Math.round(posy * scale);
                                    posx = viewerWidth/2;
                                }
                            }
                            else {
                                posy = 200;
                            }
                            centered = false;
                            zoom([posx,posy],scale);
                            centered = true;
                        }
                         //basically a way to get the path to an object
                        var searchTree = function(obj, path) {
                            if (searchFound(obj)) { //if search is found return, add the object to the path and return it
                                path.push(obj);
                                return path;
                            } else if (obj.children || obj._children) { //if children are collapsed d3 object will have them instantiated as _children
                                var children = (obj.children) ? obj.children : obj._children;
                                for (var i = 0; i < children.length; i++) {
                                    path.push(obj); // we assume this path is the right one
                                    var found = searchTree(children[i], path);
                                    if (found) { // we were right, this should return the bubbled-up path from the first if statement
                                        return found;
                                    } else { //we were wrong, remove this parent from the path and continue iterating
                                        path.pop();
                                    }
                                }
                            } else { //not the right object, return false so it will continue to iterate in the loop
                                return false;
                            }
                        },
                        openPaths = function(paths) {
                            for (var i = 0; i < paths.length; i++) {
                                if (paths[i].id !== "0") { //i.e. not root
                                    paths[i].class = 'accent';
                                    if (paths[i]._children) { //if children are hidden: open them, otherwise: don't do anything
                                        paths[i].children = paths[i]._children;
                                        paths[i]._children = null;
                                    }
                                    update(paths[i]);
                                }
                            }
                        };

                        function update(source) {
                            var levelWidth = [1]
                            var childCount = function(level, n) {
                                if (n.children && n.children.length > 0) {
                                    if (levelWidth.length <= level + 1) levelWidth.push(0);

                                    levelWidth[level + 1] += n.children.length;
                                    n.children.forEach(function(d) {
                                        childCount(level + 1, d);
                                    });
                                }
                            };
                            childCount(0, root);
                            var newHeight = d3.max(levelWidth) * 45; // 25 pixels per line
                            tree = tree.size([newHeight, viewerWidth]);

                            nodes = tree.nodes(root).reverse();
                            nodes.forEach(function(node) {
                                node.y = (node.depth * 110); // 100px per level.
                            });
                            links = getExtraLinks(nodes);

                            nodes.forEach(function(d) {
                                d.y = (d.depth * (maxLabelLength * 10)); //maxLabelLength * 10px
                            });

                            var node = childGroup.selectAll("g.node")
                                .data(nodes, function(d) {
                                    return d.id || (d.id = ++i);
                                });

                            var nodeEnter = node.enter().append("g")
                                .attr("class", "node")
                                .attr("data-nw-node",function(d) {
                                    return d.id
                                })
                                .attr("transform", function(d) {
                                    return "translate(" + source.y0 + "," + source.x0 + ")";
                                })
                                .on('click', click)                        
                                .on('dblclick', dblClick);

                            nodeEnter.append("path")
                                .attr('class', function(d) {
                                    var cls = 'nodePath  ' + d.id + ' src-' +d.type + ' ';
                                    if (showPath && inFilter) {
                                        cls += d.found || ' lighten';
                                    }
                                    return cls;
                                })
                                .attr("d", function(d) {
                                    var t = d.type ? d.type.substr(0, 1) : DEFAULT;
                                    return shape[t] ? shape[t] : shape[DEFAULT];
                                })
                                .style("stroke", function(d) {
                                    return d.status ? status_colors[d.status] : status_colors[DEFAULT];
                                })
                                .style("fill", function(d) {
                                    return d._children && d._children.length ? "lightsteelblue" : "#fff";
                                })

                            nodeEnter.append("text")
                                .attr("x", function(d) {
                                    return d.children || d._children ? -10 : 10;
                                })
                                .attr("dy", ".35em")
                                .attr('class', function(d) {
                                    var cls = 'nodeText ' + 'd.id src-' + d.type + ' ';
                                    if (showPath && inFilter) {
                                        cls += d.found || ' lighten';
                                    }
                                    return cls;
                                })
                                .attr("text-anchor", function(d) {
                                    return d.children && d.children.length || d._children && d._children.length ? "end" : "start";
                                })
                                .text(function(d) {
                                    return d.name.substring(0, MAX_TEXT_SIZE);
                                })
                                .style("fill-opacity", 0)

                            node.select('text')
                                .attr("x", function(d) {
                                    return d.children && d.children.length || d._children && d._children.length ? -10 : 10;
                                })
                                .attr("text-anchor", function(d) {
                                    return d.children && d.children.length || d._children && d._children.length ? "end" : "start";
                                })
                                .text(function(d) {
                                    return d.name.substring(0, MAX_TEXT_SIZE);
                                })

                            node.select("path.nodePath")
                                .attr("r", 10)
                                .style("fill", function(d) {
                                    // return d.status ? status_colors[d.status] : status_colors[DEFAULT];
                                    return d._children && d._children.length ? "lightsteelblue" : "#fff";
                                });

                            var nodeUpdate = node.transition()
                                .duration(duration)
                                .attr("transform", function(d) {
                                    return "translate(" + d.y + "," + d.x + ")";
                                });
                          
                            nodeUpdate.select("text")
                                .style("fill-opacity", 1);

                            var nodeExit = node.exit().transition()
                                .duration(duration)
                                .attr("transform", function(d) {
                                    return "translate(" + source.y + "," + source.x + ")";
                                })
                                .remove();

                            nodeExit.select("path.nodePath")
                                .attr("size", 0);

                            nodeExit.select("text")
                                .style("fill-opacity", 0);


                            var link = childGroup
                                        .selectAll("path.link")
                                        .data(links, function(d) { return d.target.id; });

                            //link = link.data(links, function(d) { return d.target.id; });

                            link.enter().insert("path", "g")
                                .attr("class", function(d) {
                                    var cls = 'link src-' 
                                            + d.source.type 
                                            + ' tgt-' 
                                            + d.target.type + ' ' 
                                            + 'tgt-'+d.target.status + ' '
                                            + 'src-'+d.source.status + ' '
                                    if (showPath && inFilter) {
                                        cls += d.found || ' lighten ';
                                    }
                                    return cls;
                                })
                                .attr("src", function(d) {
                                    return d.source.id
                                })
                                .attr("tgt", function(d) {
                                    return d.target.id
                                })
                                .attr("d", function(d) {
                                    var o = {
                                        x: source.x0,
                                        y: source.y0
                                    };
                                    return diagonal({
                                        source: o,
                                        target: o
                                    });
                                })                                
                            link.transition()
                                .duration(duration)
                                .attr("d", diagonal);
                            link.exit().transition()
                                .duration(duration)
                                .attr("d", function(d) {
                                    var o = {
                                        x: source.x0,
                                        y: source.y0
                                    };
                                    return diagonal({
                                        source: o,
                                        target: o
                                    })
                                })
                                .remove();
                            
                            maxX = $("#scroll-box").width();
                            maxY = $("#scroll-box").height();
                            nodes.forEach(function(d) {
                                d.x0 = d.x;
                                d.y0 = d.y;
                                if (d.x > maxY) {
                                    maxY = d.x;
                                }
                                if (d.y > maxX) {
                                    maxX = d.y;
                                }
                            });
                           
                            console.log(maxY, maxX);
                        }
                        
                        expandAll(root);
                        svgWrapper
                            .attr("width", maxX + "px")
                            .attr("height", maxY + "px");

                        $("#scroll-box .scroll-content").width(maxX).height(maxY);
                        
                        $('#rotate-button').click();
                       
                        if (crit && crit.length && showPath) {
                            var paths = searchTree(root, []);
                            if (typeof(paths) !== "undefined") {
                                openPaths(paths);
                            }
                        }
                    };
                    
                if (!jsonTree) {
                    rootName = $rootScope.currentNetwork ? $rootScope.currentNetwork.name : 'ROOT';
                    filterArray = flatTreeArray;
                    if (!initSearch) {
                        $('#filter-select').select2({
                            tags: "true",
                            placeholder: "Search/Filter",
                            width: "400px"
                        });
                        $("#filter-select-wrap input, #filter-select-wrap select").on("change", function() {
                            $scope.search.crit = $(this).val();
                        });
                        $('#apply-button').on('click', function() {
                            crit = $scope.search.crit;
                            showPath = document.getElementById('show-path').checked;
                            var t;
                            $('#filter-select').next()
                                .find('.select2-selection__choice')
                                .each(function(e) {
                                    t = $(this).text().trim().substring(1);
                                    t = t.replace(/[^\w\s]/gi, '')
                                    if (!crit) {
                                        crit = [t];
                                    }
                                    if (!~crit.indexOf(t)) {
                                        crit.push(t);
                                    }
                                });
                            if (crit && crit.length) {
                                crit = crit.map(function(c) {
                                    if (~$scope.search.types.indexOf(c)) {
                                        return {
                                            type: 'type',
                                            val: c
                                        }
                                    } else if (~$scope.search.statuses.indexOf(c)) {
                                        return {
                                            type: 'status',
                                            val: c
                                        }
                                    } else {
                                        return {
                                            type: 'name',
                                            val: c.toUpperCase()
                                        }
                                    }
                                });

                            }
                            if (prevFilter) {
                                prevFilter = false;
                                if (!crit || !crit.length) {
                                    jsonTree = JSON.parse(JSON.stringify(origJsonTree));
                                    inFilter = false;
                                    showDND();
                                    return;
                                }
                            }
                            if (crit && crit.length) {
                                inFilter = true;
                                $scope.search.crit = crit.slice(0);
                                if (showPath) {
                                    jsonTree = JSON.parse(JSON.stringify(origJsonTree));
                                    prevShowPath = true;

                                } else {
                                    filterResult();
                                    prevShowPath = false;
                                }
                                showDND();
                                prevFilter = true;
                            }
                            else {
                                $scope.search.crit = false;
                            }
                        });
                        initSearch = true;
                    }
                    filterResult();
                    showDND();
                }
                closeBubble();
                $('#lock-unlock').hide();
            }
            $scope.showNetwork = function() {
                closeBubble();
                console.log('showing Network');
                $('#lock-unlock').show();
            }
            var nodeTree = function() {
                this.searchTrees = function(trees, id) {
                    var found = false;
                    for (var i = 0; found === false && i < trees.length; i++) {
                        found = this.findChild(trees[i], id);
                    }
                    return found;
                }
                this.findChild = function(tree, id) {
                    if (tree.id == id) {
                        return tree;
                    } else if (tree.children != null) {
                        var result = false;
                        for (var i = 0; result === false && i < tree.children.length; i++) {
                            result = this.findChild(tree.children[i], id);
                        }
                        return result;
                    }
                    return false;
                }
                this.getChildren = function(children, tree, depth, nodeArr) {
                    var childs = [];
                    for (var key in children) {
                        var childId = children[key];

                        for (var i = 0; i < tree.length; i++) {
                            if (tree[i].id == childId) {
                                var node = jQuery.extend({}, tree[i]);
                                if (nodeArr.indexOf(childId) !== -1) //avoid recursive call for TopologyRing
                                    return;
                                nodeArr.push(childId);
                                node.depth = depth;
                                if (node.children.length > 0) {
                                    node.children = this.getChildren(node.children, tree, depth + 1, nodeArr);
                                }
                                childs.push(node);
                            }
                        }
                    }
                    return childs;
                }
                this.makeTree = function(tree) {
                    var roots = [];
                    for (var i = 0; i < tree.length; i++) {
                        if (tree[i].parents.length == 0) {
                            var node = $.extend({}, tree[i]);

                            node.depth = 1;
                            if (node.children.length > 0) {
                                var nodeArr = []; //to  avoid recursive call for TopologyRing
                                node.children = this.getChildren(node.children, tree, 2, nodeArr);
                            }
                            roots.push(node);
                        }
                    }
                    return roots;
                }
            }

            $scope.treeManager = new nodeTree();

            $scope.$on('$routeChangeStart', function(angularEvent, next, current) {
                //window.clearInterval($scope.mapRefresh);
                window.clearTimeout($scope.updateTimer);
            });

            $scope.forceRefreshTopology = function() {
                //$window.location.reload();
                //updateTopology();
                $rootScope.currentTab = $scope.currentTab;
                $route.reload();
            };

            function strHash(myString) {
                var hash = 0,
                    i, chr, len;
                if (myString.length == 0)
                    return hash;
                for (i = 0, len = myString.length; i < len; i++) {
                    chr = myString.charCodeAt(i);
                    hash = ((hash << 5) - hash) + chr;
                    hash |= 0; // Convert to 32bit integer
                }
                return hash;
            }
            $scope.activateTab = function(tab) {
                $scope.currentTab = tab;
                $('.nav-tabs a[data-target="#' + tab + '-content"]').tab('show');
                switch (tab) {
                    case 'network':
                        $scope.showNetwork();
                    case 'tree':
                        $scope.showTree();
                }
            };

            $.getJSON("/users/current", function(currUser) {
                orgId = $scope.currentUser.defaultOrg.id || currUser.defaultOrg.id;

                //$("#top-nav-container").height() - $("#topGraph").closest(".section-title").height();

                d3.selection.prototype.moveToFront = function() {
                    return this.each(function() {
                        this.parentNode.appendChild(this);
                    });
                };
                var zoom = d3.behavior.zoom().scaleExtent([0.5, 10]).on("zoom", rescale);

                var force = d3.layout.force()
                    .size([width, height])
                    .nodes([]) // initialize with a single node
                    .linkDistance(1)
                    .charge(-500)
                    .on("tick", tick);

                var drag = force.drag()
                    .on("dragstart", dragstarted)
                    .on("drag", dragged)
                    .on("dragend", dragended);

                function dragstarted(d) {
                    if ($scope.lock) return false;

                    d3.event.sourceEvent.stopPropagation();
                    var sel = d3.select(this);
                    sel.classed("dragging", true);
                    sel.classed("fixed", d.fixed = true);

                }

                function dragged(d) {
                    if ($scope.lock) {
                        return false;
                    }

                    //window.clearInterval($scope.mapRefresh);

                    d3.select(this).attr("transform", function(d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    });
                    d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);

                    $scope.didDrag = true;
                    $scope.currentDevice = null;
                    $scope.$apply();
                }

                function dragended(d) {
                    var sel = d3.select(this);
                    $scope.currentDevice = d;
                    console.log('Current Device ' + d);
                    $scope.$apply();
                    sel.classed("dragging", false);
                    if ($scope.didDrag) {
                        $scope.didDrag = false;
                        $.ajax({
                            url: "/" + orgId + "/deviceGeoInfo?" + (new Date().getTime()),
                            type: "put",
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify([{
                                id: d.id,
                                x: Math.round(d.x),
                                y: Math.round(d.y)
                            }]),
                            timeout: 10000,
                            async: true,
                            success: function(json, textStatus, XMLHttpRequest) {
                                // changing the values for settin the refresh time to 2 mins, previous value was 5000
                                //$scope.mapRefresh = window.setInterval(update, 120000);
                                // clone for tree
                                flatTreeArray = JSON.parse(JSON.stringify(json));
                                jsonTree = null;
                                origJsonTree = null;
                                if (typeof json === "string" || json.indexOf('ng-app="mimosaSignupApp"') !== -1 || json.indexOf('errorMessage') !== -1) {
                                    var url = $location.url(); //change path to url to  get url with its contains parameters
                                    $window.location.replace("/app/welcome.html#/login?destPath=" + encodeURIComponent(url));
                                    return {};
                                }
                                $scope.search = {
                                    names: [],
                                    types: [],
                                    statuses: []
                                }
                                newSearch = true;
                                updateTopology();
                                if (!$scope.currentTab) {
                                    $scope.currentTab = $rootScope.currentTab;
                                }
                                console.log('Current Tab @1367 ' + $scope.currentTab);
                                if ($scope.currentTab) {
                                    $scope.activateTab($scope.currentTab);
                                }
                            },
                            error: function(json, textStatus, XMLHttpRequest) {
                                // changing the values for settin the refresh time to 2 mins, previous value was 5000
                                //$scope.mapRefresh = window.setInterval(update, 120000);
                                updateTopology();
                            }
                        });
                    }
                }

                function tick(e) {
                    var ky = e.alpha;
                    links.forEach(function(d, i) {
                        if (d.target) {
                            if (d.target.fixed)
                                return;
                            var target = $scope.treeManager.searchTrees(tree, d.target.id);

                            d.target.y += (target.depth * 100 - d.target.y) * 5 * ky;
                        }
                    });
                    link.attr("x1", function(d) {
                            if (d.source) {
                                return d.source.x;
                            }
                        })
                        .attr("y1", function(d) {
                            if (d.source) {
                                return d.source.y;
                            }
                        })
                        .attr("x2", function(d) {
                            if (d.target) {
                                return d.target.x;
                            }
                        })
                        .attr("y2", function(d) {
                            if (d.target) {
                                return d.target.y;
                            }
                        });

                    node.attr("transform", function(d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    });
                    force.stop();
                }
                
                function rescale() {
                    trans = d3.event.translate;
                    scale = d3.event.scale;

                    vis.attr("transform",
                        "translate(" + trans + ")" + " scale(" + scale + ")");
                }
                
                /*
                function rescale() {

                     var e = d3.event,
                    // now, constrain the x and y components of the translation by the
                    // dimensions of the viewport
                    tx = Math.min(0, Math.max(e.translate[0], width - width * e.scale)),
                    ty = Math.min(0, Math.max(e.translate[1], height - height * e.scale));
                    // then, update the zoom behavior's internal translation, so that
                    // it knows how to properly manipulate it on the next movement
                    zoom.translate([tx, ty]);
                    // and finally, update the <g> element's transform attribute with the
                    // correct translation and scale (in reverse order)
                    vis.attr("transform", [
                        "translate(" + [tx, ty] + ")",
                        "scale(" + e.scale + ")"
                    ].join(" "));
                }*/
                var outer = d3.select("#network-content")
                    .append("svg:svg")
                    .attr("width", width)
                    .attr("height", height)
                    .call(zoom)
                    .on("dblclick.zoom", null)
                    .attr("pointer-events", "all");
                var filter = outer.append("defs").append("filter")
                    .attr("id", "blur");

                filter.append("feGaussianBlur")
                    .attr("in", "SourceAlpha")
                    .attr("stdDeviation", "2.2");

                filter.append("feOffset")
                    .attr("dx", 0)
                    .attr("dy", 1)
                    .attr("result", "offsetblur");
                filter.append("feFlood")
                    .attr("flood-color", "rgba(0,0,0,0.5)");
                filter.append("feComposite")
                    .attr("in2", "offsetblur")
                    .attr("operator", "in");

                var merge = filter.append("feMerge");
                merge.append("feMergeNode");
                merge.append("feMergeNode")
                    .attr("in", "SourceGraphic");

                var vis = outer
                    .append('svg:g')
                    .append('svg:g');

                var triangle = {
                        width: 20,
                        height: 10
                    },
                    nodes = force.nodes(),
                    links = force.links();

                force.start();

                var node = vis.selectAll(".node"),
                    link = vis.selectAll(".link");

                redraw();
                var lastHash = 0;

                function update() {
                    if (!$scope.lock) {
                        //                  console.log("Unlocked for editing. Not updating");
                        return;
                    }
    
                    $.ajax({
                        url: "/" + orgId + "/deviceGeoInfo?" + (new Date().getTime()),
                        type: "get",
                        async: false,
                        timeout: 10000,
                        success: function(json, textStatus, XMLHttpRequest) {
                            
                            if (typeof json === "string" || json.indexOf('ng-app="mimosaSignupApp"') !== -1 || json.indexOf('errorMessage') !== -1) {
                                var url = $location.url(); //change path to url to  get url with its contains parameters
                                $window.location.replace("/app/welcome.html#/login?destPath=" + encodeURIComponent(url));
                                return {};
                            }

                            force.nodes([]);
                            force.links([]);
                            if (json.length > 0) {
                                $scope.currentTime = new Date();
                            }
                            $scope.$apply();

                            flatTreeArray = JSON.parse(JSON.stringify(json));
                            jsonTree = null;
                            
                            $rootScope.oldName = $rootScope.currentNetwork.name;
                            $scope.search = {
                                names: [],
                                types: [],
                                statuses: []
                            }
                            newSearch = true;
            
                            json.forEach(function(e, i) {
                                var idx = $scope.selectedDevices.indexOf(e);
                                if (idx !== -1) {
                                    e.selected = true;
                                } else {
                                    e.selected = false;
                                }

                                if ($scope.currentDevice && $scope.currentDevice.id === e.id) {
                                    $scope.currentDevice = e;
                                }
                                if ($scope.summaryDevice && $scope.summaryDevice.id === e.id) {
                                    $scope.summaryDevice = e;
                                }
                            });

                            tree = $scope.treeManager.makeTree(json);
                            // this ajax response is different, continue

                            var current_nodes = [];
                            var delete_nodes = [];
                            var jsonVal, parentChild = {};
                            var svgInst = $("svg");
                            for (var i = 0, pid = 0, len = json.length; i < len; i++) {
                                jsonVal = json[i];
                                if (jsonVal.parents.length === 0) {
                                    jsonVal.fixed = true;
                                    jsonVal.y = jsonVal.y || 50;
                                    jsonVal.x = jsonVal.x ||
                                        (svgInst.width() / 2) - (100 * tree.length / 2) + (100 * pid);
                                    pid++;
                                } else {
                                    if (parentChild[jsonVal.parents] != null || parentChild[jsonVal.parents] != undefined) {
                                        parentChild[jsonVal.parents] = parentChild[jsonVal.parents] + 1;
                                    } else {
                                        parentChild[jsonVal.parents] = 0;
                                    }
                                    jsonVal.x = jsonVal.x || svgInst.width() / 2 + 50 * parentChild[jsonVal.parents];
                                    jsonVal.y = jsonVal.y || 100;
                                }

                                if (jsonVal.x && jsonVal.y) {
                                    jsonVal.fixed = true;
                                }
                            }
                            parentChild = {};
                            $.each(json, function(i, data) {

                                var result = $.grep(nodes, function(e) {
                                    return e.id == data.id;
                                });
                                if (!result.length) {
                                    nodes.push(data);
                                } else {
                                    var pos = nodes.map(function(e) {
                                        return e.id;
                                    }).indexOf(data.id);
                                    nodes[pos].severity = data.severity;
                                    nodes[pos].monitored = data.monitored;
                                    nodes[pos].x = data.x;
                                    nodes[pos].y = data.y;
                                }
                                current_nodes.push(data.id);
                            });

                            $.each(nodes, function(i, data) {
                                if (current_nodes.indexOf(data.id) === -1) {
                                    delete_nodes.push(data.index);
                                }
                            });
                            $.each(delete_nodes, function(i, data) {
                                nodes.splice(data, 1);
                            });
                            var nodeMap = {};
                            nodes.forEach(function(x) {
                                nodeMap[x.id] = x;
                            });

                            var nodeLinks = [];

                            $scope.devices = {};
                            for (var i = 0; i < json.length; i++) {
                                var n = json[i];
                                if (n.children.length > 0) {
                                    for (var j = 0; j < n.children.length; j++) {
                                        var child = n.children[j];

                                        nodeLinks.push({
                                            source: nodeMap[n.id],
                                            target: nodeMap[child],
                                            colour: "#000"
                                        });
                                    }
                                }

                                $scope.devices[n.id] = n;
                            }
                            links = nodeLinks.map(function(x) {
                                //                         console.log("x = " ,x);
                                if (!x.source || !x.target) {
                                    return {};
                                }
                                return {
                                    colour: x.colour,
                                    source: x.source,
                                    target: x.target,
                                    severity: x.source.severity

                                }
                            });
                            $('#loading').fadeOut(300);
                            redraw();
                            // force.start();

                            // $scope.retryCount = 0;
                            $scope.error = false;

                            // $scope.devices = json;
                            $scope.$apply();
                            if (!$scope.currentTab) {
                                $scope.currentTab = $rootScope.currentTab;
                            }
                            console.log('Current Tab at #1622 ' + $scope.currentTab);
                            
                            if ($scope.currentTab) {
                                $scope.activateTab($scope.currentTab);
                            }
                        },
                        error: function(xhr, message, exp) {
                            window.clearInterval($scope.mapRefresh);
                            $scope.error = true;
                            $scope.retryAfter = 20;
                            $scope.$apply();
                            window.setTimeout(waitAndCheck, 1000);
                        }
                    });
                }
                //update();
                if (!$scope.mapRefresh) {
                    // changing the values for settin the refresh time to 2 mins, previous value was 5000
                    updateTopology();
                }

                $scope.updateTimer = null;

                function updateTopologyTimer() {
                    update();
                    $scope.updateTimer = window.setTimeout(updateTopology, 120000);
                }

                function updateTopology() {
                    window.clearTimeout($scope.updateTimer);
                    updateTopologyTimer();
                }

                function getLineColor(src, dest) {
                    if (src && dest) {
                        if (!src.push && !src.monitored) {
                            var srcState = "notmonitored";
                        } else {
                            var srcState = src.severity.toLowerCase();
                        }

                        if (!dest.push && !dest.monitored) {
                            destState = "notmonitored";
                        } else {
                            var destState = dest.severity.toLowerCase();
                        }

                        if (srcState === "failed" && destState === "failed") {
                            return 'failed';
                        } else if (srcState === 'warning' || destState === 'warning') { // SJS : added condition to include state 'warning'
                            return 'ok';
                        } else if (srcState === "notmonitored" && destState === "notmonitored") {
                            return 'notmonitored';
                        } else if ((srcState === "notmonitored" && destState === "failed") || (srcState === "failed" && destState === "notmonitored")) {
                            return 'failed';
                        } else if (srcState === "ok" && destState === "ok") {
                            return 'ok';
                        } else if ((srcState === "failed" && destState === "ok") || (srcState === "ok" && destState === "failed")) {
                            return 'ok';
                        } else if ((srcState === "notmonitored" && destState === "ok") || (srcState === "ok" && destState === "notmonitored")) {
                            return 'notmonitored';
                        }
                    } else {
                        return 'notmonitored';
                    }
                }

                function waitAndCheck() {
                    //window.clearInterval($scope.mapRefresh);

                    $scope.retryAfter -= 1;
                    if ($scope.retryAfter <= 0) {
                        $scope.error = false;
                        // changing the values for settin the refresh time to 2 mins, previous value was 5000
                        window.setTimeout(update, 0); //NMS-893 - SJS
                    } else {
                        window.setTimeout(waitAndCheck, 1000);
                    }
                    $scope.$apply();
                }

                function redraw() {

                    link = link.data(links);
                    link.enter().append("line");
                    link.attr('class', function(d) {
                        return getLineColor(d.source, d.target);
                    }).attr("source_node", function(d) {
                        if (d.source) {
                            return d.source.id;
                        }
                    });
                    link.exit().remove();


                    node = node.data(nodes, function(d) {
                        return d.id;
                    });
                    var wrapper = node.enter().insert("g");

                    wrapper.attr("data-nw-node", function(d) {
                        return d.id;
                    });

                    wrapper.call(drag)
                        .on("mouseover", function(node) {
                            $scope.summaryDevice = node;
                            $scope.$apply();
                        })
                        .on("click", function(node) {
                            /*
                             if($scope.lock) {
                             //alert("Please unlock to select devices");
                             return;
                             }
                             */
                            // window.location.assign("/app/index.html#/devices/select/"+ node.id);


                            $scope.currentDevice = node;
                            $scope.$apply();

                        })
                        .on("mouseout", function() {
                            $scope.summaryDevice = $scope.currentDevice;
                            $scope.$apply();

                            d3.select(this).select("g.labelBox").attr("visibility", null);
                        })
                        .on("dblclick", function(d) {
                            d3.select(this).classed("fixed", (d.fixed = true));
                        });

                    node.attr("id", function(d) {
                        return "node_" + (d.id);
                    });
                    node.attr("class", function(d) {

                        // NMS-892 - SJS
                        if (!(d.monitored || d.push)) {
                            return 'notmonitored';
                        }
                        var severity = d.severity.toLowerCase();
                        return (d.push || d.monitored) ? (severity == "warning" || severity == "critical") ? "ok" : severity : "notmonitored"; // fixing the topology node color issue
                    });
                    wrapper.append("circle").attr("r", 5);


                    var labelBox = wrapper.append("g");
                    var label = labelBox.append("path");
                    var labelText = labelBox.append("text");
                    labelBox.attr("class", "labelBox");

                    labelText.text(function(d) {
                        return d.friendlyName;
                    });

                    labelBox.attr("visibility", "visible")
                        .attr("transform", function(d) {
                            var dimensions = this.getBBox();
                            return "translate(" + (0 - ((dimensions.width / 2)) - 5) + "," + (0 - (dimensions.height + 19)) + ")";
                        });

                    labelText.attr("x", 4).attr("y", function() {
                        return this.getBBox().height - 2;
                    });
                    label.each(function(d) {
                        var dimensions = d3.select(this.parentNode).select("text").node().getBBox();
                        var width = dimensions.width + 10;
                        var height = dimensions.height + 4;

                        d3.select(this).attr({
                            d: "m 0, 0 h " + (width) + " v " + (height) + " h " + (0 - ((width / 2) - (triangle.width / 2))) + " l " + [0 - (triangle.width / 2), triangle.height].join(",") + " l " + [0 - (triangle.width / 2), 0 - triangle.height].join(",") + " H 0 z",
                            x: 0,
                            y: 0,
                            class: "infobox",
                            "stroke-linejoin": "round"
                        });

                    });
                    /*var infoBox = wrapper.append("g");
                     var box = infoBox.append("path");
                     var textBox = infoBox.append("text");
                     var infoFields = {
                     "Name": "friendlyName",
                     "Model": "modelName",
                     "Manufacturer": "manufacturerName",
                     "IP Address": "ipAddress"
                     };

                     var count = 0;
                     for (var index in infoFields) {
                     var label = index;
                     var key = infoFields[index];
                     textBox.append("tspan")
                     .text(function(d) {
                     return label + ": "
                     })
                     .attr("text-anchor", "end")
                     .attr("dy", "1em")
                     .attr("y", function(d) {
                     return 20 * count;
                     })
                     .attr("x", function(d) {
                     return 120;
                     });

                     textBox.append("tspan")
                     .attr("dy", "1em")
                     .text(function(d) {
                     return d[key] + "  ";
                     })
                     .attr("y", function(d) {
                     return 20 * count;
                     })
                     .attr("x", function(d) {
                     return 125;
                     });
                     count++;
                     }*/
                    /*infoBox
                     .attr("class", "infoBox")
                     .attr("visibility", "hidden")
                     .attr("transform", function(d) {
                     var dimensions = this.getBBox();

                     return "translate(" + (0 - ((dimensions.width / 2))) + "," + (0 - (dimensions.height + 15)) + ")";
                     });

                     //         infoBox.attr("filter", "url(#blur)");
                     //         labelBox.attr("filter", "url(#blur)");

                     box.each(function(d) {
                     var dimensions = d3.select(this.parentNode).node().getBBox();

                     d3.select(this).attr({
                     d: "m 0, 0 h " + dimensions.width + " v " + dimensions.height + " h " + (0 - ((dimensions.width / 2) - (triangle.width / 2))) + " l " + [0 - (triangle.width / 2), triangle.height].join(",") + " l " + [0 - (triangle.width / 2), 0 - triangle.height].join(",") + " H 0 z",
                     x: 0,
                     y: 0,
                     class: "infobox"
                     });
                     });*/
                    node.exit().remove();

                    force.start();

                    //                    nodes = null;
                    //                    links = null;
                }

            });
            $scope.isDuplicateNetworkName = function(network) {
                return CheckedDataTypes.isDuplicateNetworkName($scope.networks, network)
            };
        }
    ]);


    app.controller("EditNodeController", ["$scope", "$http", "DeviceService", "$window", function($scope, $http, DeviceService, $window) {

        $scope.setDeviceMonitoring = function(enable) {
            var dev = $scope.currentDevice;
            var url = "/" + $scope.currentUser.defaultOrg.id + "/devices/" + dev.id + "/actions/schedule/";
            $http.post(url + "?enable=" + enable)
                .success(function(res) {
                    $window.location.reload();
                });
        };

        $scope.applyActionProfile = function(actionProfile) {
            var dev = $scope.currentDevice;
            $scope.confirm("Do you want to apply new action profile?", function(val) {
                if (!val) {
                    return;
                }
                $http.put('/' + $scope.currentUser.defaultOrg.id + '/devices/' + dev.id + '/actionProfile/', actionProfile)
                    .success(function(data, status) {
                        if (data == 'errorMessage:ActionProfile not supported') {
                            $scope.alert("ActionProfile not supported");
                            return;
                        }
                        $window.location.reload();
                    });
            });
        };

        $scope.deleteDevice = function() {
            var dev = $scope.currentDevice;

            $scope.confirm("Delete device " + $scope.currentDevice.friendlyName, function(val) {
                if (!val) {
                    return;
                }

                $http.delete("/" + $scope.currentUser.defaultOrg.id + "/devices/" + dev.id).success(function() {
                    $scope.alert("Device deleted successfully!!", function() {
                        //fetchDevices($scope.currentUser);
                        $window.location.reload();
                    });
                }).error(function(data) {
                    $scope.alert("Device scheduled for upgrade cannot be deleted or Device not found, Try after upgrade is finished.");
                });

                //                DeviceService.deleteDevice($scope.currentUser.defaultOrg.id, dev.id, function(data) {
                //                    console.log("data",data);
                //                    $scope.alert("Device deleted successfully", function() {
                //                      $window.location.reload();
                //                    });
                //                });
            });
        };

        $scope.currentDeviceName = function() {
            $scope.$emit('currentDeviceNameEvnt', $scope.currentDevice);
        };
    }]);


    app.controller("BulkEditController", ["$scope", "$http", "$window", function($scope, $http, $window) {

        $scope.applyActionProfile = function(actProfile) {
            var selectedDevices = $scope.selectedDevices;
            if (selectedDevices.length === 0) {
                $scope.alert("Select device for delete.");
                return;
            }
            var lstIds = "";
            for (var i = 0; i < selectedDevices.length; i++) {
                lstIds = lstIds + "id=" + selectedDevices[i].id + "&";
            }

            $scope.confirm("Apply this action profile", function(val) {
                if (!val) {
                    return;
                }
                $http.put('/' + $scope.currentUser.defaultOrg.id + '/devices/actionProfile?' +
                    lstIds + 'actionProfileName=' + actProfile).success(function(data, status) {
                    $window.location.reload();
                });
            });
        };

        $scope.monitorDevices = function(enable) {
            var selectedDevices = $scope.selectedDevices;
            if (selectedDevices.length === 0) {
                $scope.alert("Select device for monitoring.");
                return;
            }
            var lstIds = "";
            for (var i = 0; i < selectedDevices.length; i++) {
                lstIds = lstIds + "id=" + selectedDevices[i].id + "&";
            }

            $scope.confirm("Confirm bulk monitoring", function(val) {
                if (!val) {
                    return;
                }
                $http.put('/' + $scope.currentUser.defaultOrg.id + '/devices?' + lstIds + 'enable=' + enable).success(function(data, status) {
                    if (data.length > 0) {
                        var msg = (enable === 'true') ? 'monitored' : 'unmonitored';
                        $scope.alert("Device " + msg + " successfully!!", function() {
                            $window.location.reload();
                        });
                    }
                });
            });
        };


        $scope.deleteDevices = function() {
            var selectedDevices = $scope.selectedDevices;
            if (selectedDevices.length === 0) {
                $scope.alert("Select devices for deleting.");
                return;
            }
            var lstIds = "";
            for (var i = 0; i < selectedDevices.length; i++) {
                lstIds = lstIds + "id=" + selectedDevices[i].id + "&";
            }

            $scope.confirm("Confirm bulk deletion", function(val) {
                if (!val) {
                    return;
                }
                $http.delete("/" + $scope.currentUser.defaultOrg.id + "/devices?" + lstIds).success(function() {
                    $scope.alert("Devices deleted successfully!!");
                    $window.location.reload();
                }).error(function(data) {
                    $scope.alert(data.message);
                });
            });
        };

    }]);

})(angular, mimosaApp);

//NMS-894 - SJS
$(document).ready(function() {
    $.ajaxSetup({ cache: false });
});
