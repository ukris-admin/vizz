"use strict";
//http://stackoverflow.com/questions/20797521/increasing-gap-between-nodes-of-my-d3-tree-layout
//https://github.com/flowhub/the-graph.git
//http://codepen.io/mikefab/pen/IDdt
//https://bl.ocks.org/curran/3c9fe2992201a514e802
//http://stackoverflow.com/questions/17558649/d3-tree-layout-separation-between-nodes-using-nodesize
//https://bl.ocks.org/jkeohan/b8a3a9510036e40d3a4e
//http://stackoverflow.com/questions/18416749/adding-fontawesome-icons-to-a-d3-graph
//http://jsfiddle.net/xg8fLs26/4/
//https://jsfiddle.net/MDragon/nxjeen1u/
(function() {

    function jsonStringify(json) {
        return JSON.stringify(json, undefined, 1);
    }

    function random(min, max) {
        return (Math.floor(Math.random() * max) + min);
    }

    function isEqual(src, tgt) {
        if (!src.children) {
            src.children = [];
        }
        if ((src.name !== tgt.name) || (src.children.length !== tgt.children.length)) {
            return false;
        }
        if (src.children) {
            src.children.sort(function(a, b) {
                return (a.name > b.name);
            })
            tgt.children.sort(function(a, b) {
                return (a.name > b.name);
            })
            var i;
            for (i = 0; i < src.children.length; i++) {
                if (!isEqual(src.children[i], tgt.children[i])) {
                    return false;
                }
            }
        }
        return true;
    }
    /*=======================================
      * Below functions to create Test Case
     ========================================*/

    var flatTreeArray = [],
        jsonTree,
        onlyJSON,
        testCaseNo = 1,
        fileName,
        flatOutputFile,
        jsonOutputFile,
        $flatFileContent,
        $treeContent,
        treeContent = '#tree-content',
        mtreeContent = '#mtree-content',
        networkContent = '#network-content',
        $networkContent = $(networkContent),
        $treeContent,
        $loading,
        $results,
        $downloadButton,
        origTree,
        origNodes,
        currentData,
        currentCluster,
        jsonText,
        showFullTree = 1,
        usePosCache = false,
        dataKeys = {
            'name': 'Name',
            'ipAddress': 'IP Address',
            'modelName': 'Model Name',
            'deviceType': 'Device Type',
            'manufacturerName': 'Manufacturer Name',
            'deviceType': 'Device Type',
            "status": "status",
            "serialNumber": "Serial Number",
            "swVersion": "Software Version",
            "children": "Children",
            "parents": "Parents"
        },
        optionDefaults = {
            maxDepth: 10,
            minChildren: 0,
            maxChildren: 5
        },
        tableTab,
        origTab,
        treeTab,
        mtreeTab,
        networkTab,
        radialTab,
        $uploadButton,
        searchInput = '',
        // generate Test Tree
        fullNodes,
        Links,
        Nodes,
        Many,
        rootName,
        currentTab,
        filterArray,
        DEFAULT = 'DEFAULT',
        NOT_MONITORED = 'NOT_MONITORED',
        OK = 'OK',
        FAILING = 'FAILING',

        sizes = {
            'A5C': 21,
            'A5': 18,
            'B': 13,
            'B5C': 15,
            'B5': 12,
            'C5C': 9,
            'C5': 6,
            'G2': 3,
            DEFAULT: 3
        },
        node_icons = {

        },
        status_colors = {
            FAILING: '#d01717',
            NOT_MONITORED: '#808080',
            OK: '#8bc540',
            DEFAULT: '#0000FF'
        },
        deviceTypes,
        status,
        origDeviceTypes,
        origStatus,
        SHAPE_SIZE = 100,
        symbol,
        shape,
        line_colors = {
            'A': '#FF0000',
            'A5C': '#FF0000',
            'A5': '#E76E3C',
            'B': '#800080',
            'B5C': '#800080',
            'B5': '#58007E',
            'C': '#008000',
            'C5C': '#008000',
            'C5': '#205E3B',
            'G': '#2B2B2B',
            'G2': '#2B2B2B',
            DEFAULT: '#000'
        },
        defaultStyle = {
            padding: "0px 5px 0px 5px",
            margin: "5px",
            "border-radius": "16px",
            "background-color": "white",
            "stroke": "none",
            "cursor": "pointer"
        },
        hoveredStyle = {
            "background-color": "lightgray"
        },
        clickedStyle = {
            "background-color": "gray"
        },
        filter = function(crit) {
            filterArray = _.deepClone(flatTreeArray);
            crit = crit.trim().toUpperCase();
            filterArray = _.filter(filterArray, function(item, index) {
                return item.friendlyName.toUpperCase().indexOf(crit) >= 0;
            });
        },
        setAttr = function(node, o) {
            node.name = o.name || o.friendlyName;
            node.id = o.id;
            // node.children = o.children.slice(0);
            // node.parents = o.parents.slice(0);
            node.type = o.modelName.trim().toUpperCase();
            if (deviceTypes[node.type]) {
                deviceTypes[node.type]++;
            } else {
                deviceTypes[node.type] = 1;
            }

            if (line_colors[node.type]) {
                node.line_color = line_colors[node.type];
                node.size = sizes[node.type];
            } else {
                var a = node.type.substring(0, 2);
                if (!line_colors[a]) {
                    a = node.type.substring(0, 1);
                }
                if (line_colors[a]) {
                    node.line_color = line_colors[a];
                    node.size = sizes[a];
                } else {
                    node.line_color = line_colors[DEFAULT];
                    node.size = sizes[DEFAULT];
                }
            }
            if (!node.color) {
                if (!o.monitored) {
                    node.status = NOT_MONITORED;
                    if (status[NOT_MONITORED]) {
                        status[NOT_MONITORED]++
                    } else {
                        status[NOT_MONITORED] = 1;
                    }
                } else if (o.severity === OK) {
                    node.status = OK;
                    if (status[OK]) {
                        status[OK]++
                    } else {
                        status[OK] = 1;
                    }
                } else {
                    node.status = FAILING;
                    if (status[FAILING]) {
                        status[FAILING]++;
                    } else {
                        status[FAILING] = 1;
                    }
                }
            }
        },

        createTree = function(crit) {
            var i, node,
                o, pid, id, ps,
                fullnode,
                p, cid,
                nodes = {},
                clusterNo = 1,
                link,
                unoNodes = {},
                chilren = {},
                nodes = {},
                status = {};

            if (!crit) {
                fullNodes = {};
                filterArray = _.deepClone(flatTreeArray);
            } else {
                filter(crit);
                if (!filterArray.length) {
                    alert('No Results');
                    return false;
                }
            }
            jsonTree = {
                    id: "0",
                    name: rootName,
                    children: [],
                    size: 26
                },
                deviceTypes = {};

            for (i = 0; i < filterArray.length; i++) {
                o = filterArray[i];

                id = o.id;
                node = {};
                setAttr(node, o);
                if (!crit) {
                    fullnode = Object.assign(o, node);
                    fullNodes[id] = fullnode;
                }
                node.children = fullNodes[id].children;
                node.parents = fullNodes[id].parents;

                if (unoNodes[id]) {
                    Object.assign(unoNodes[id], node);
                } else {
                    unoNodes[id] = node;
                }
                if ((!node.parents) || (node.parents.length === 0)) {
                    node.cluster = clusterNo++;
                    jsonTree.children.push(node);
                } else {
                    if (crit) {
                        node.parents.forEach(function(pid, i) {
                            if (!unoNodes[id]) {
                                unoNodes[pid] = Object.assign(origNodes[id], {
                                    children: [node]
                                })
                            } else {
                                unoNodes[pid].children.push(node);
                            }
                        })
                    }
                }
                if (node.children && node.children.length > 0) {
                    node.children.forEach(function(cid, i) {
                        if (cid instanceof Object) {
                            cid = cid.id;
                        }
                        if (!unoNodes[cid]) {
                            if (crit) {
                                unoNodes[cid] = Object.assign(origNodes[cid], {
                                    parents: [node]
                                })
                            } else {
                                unoNodes[cid] = {
                                    id: cid
                                }
                            };
                        }
                        node.children[i] = unoNodes[cid];
                    });
                }
            }

            if (!crit) {
                origTree = jsonTree;
                origNodes = unoNodes;
                origStatus = status;
                origDeviceTypes = deviceTypes;
            }
            //console.log(fullNodes);
            return true;
        },
        filterResult = function(crit) {

            var primaryNode = {
                id: "root",
                kids: [],
                name: rootName,
                type: 'R'
            };

            var sourceNodes = {};
            deviceTypes = {};
            status = {};

            // get children for root;
            flatTreeArray.forEach(function(node) {
                var newNode = {};
                setAttr(newNode, node);
                newNode.kids = [].concat(node.children);

                if (!node.parents || !node.parents.length) {
                    newNode.parents = [primaryNode.id];
                    primaryNode.kids.push(newNode.id);
                } else {
                    newNode.parents = [].concat(node.parents);
                }

                if (!node.name) {
                    node.name = node.friendlyName;
                }
                // create dictionary for quick access;
                sourceNodes[newNode.id] = newNode;
                // clear flag;
                newNode.hasParent = false;
                node.found = false;
            });

            var findDown = function(node, found) {
                var nodeName = node.name.toLowerCase();
                if (nodeName.indexOf(crit) + 1 > 0) {
                    node.found = true;
                    return true;
                }

                if (!found && node.kids && node.kids.length) {
                    node.kids.forEach(function(kidId) {
                        var childNode = sourceNodes[kidId];
                        if (childNode) {
                            found = findDown(childNode, found);
                        }
                    });
                }
                return found;
            }

            var loadChild = function(node, nonCheck) {
                var children = [];

                if (node.kids && node.kids.length) {
                    node.kids.forEach(function(kidId) {
                        var childNode = sourceNodes[kidId];
                        // First come, first served
                        if (childNode && !childNode.hasParent) {
                            childNode = Object.assign({}, childNode);
                            sourceNodes[kidId].hasParent = true;
                            if (nonCheck || node.found) {
                                loadChild(childNode, true);
                                children.push(childNode);
                            } else {
                                if (findDown(childNode, false)) {
                                    children.push(childNode);
                                }
                                loadChild(childNode, false);
                            }
                        }
                    });
                }

                if (node.found) {
                    node._children = children;
                } else {
                    // re-assign children;
                    node.children = children;
                }
            }

            if (crit && crit.length) {
                crit = crit.toLowerCase();
                loadChild(primaryNode, false);
            } else {
                loadChild(primaryNode, true);
            }

            jsonTree = primaryNode;
        },
        showResults = function(crit) {
            toggleLoading();
            jsonTree = null;
            var ok = createTree(crit);
            if (ok) {
                networkTab = false;
                treeTab = false;
                $treeContent.html('');
                showDND();
                toggleLoading();
            }
        },
        setUpload = function() {
            // set clickevents for Upload test file
            var file, fileReader,
                $input = $('#input-file'),
                data,
                file,
                fpath;

            $input.on('change', function(evt) {
                file = evt.target.files[0];

                if (file) {
                    fpath = this.value;
                    $uploadButton.removeClass('hide');
                } else
                    $uploadButton.addClass('hide');
            });
            $uploadButton.on('click', function() {
                fpath = fpath.replace(/\\/g, '/');
                fileName = fpath.substring(fpath.lastIndexOf('/') + 1, fpath.lastIndexOf('.'));
                fileReader = new FileReader();
                fileReader.onload = function(f) {

                    var IS_JSON = true,
                        json;
                    try {
                        json = $.parseJSON(fileReader.result);
                    } catch (err) {
                        IS_JSON = false;
                    }
                    if (!IS_JSON) {
                        alert("File Is Not JSON");
                    } else {
                        $uploadButton.addClass('hide');
                        if (json instanceof Array) {
                            onlyJSON = true;
                            flatTreeArray = json;
                            origTree = false;
                        } else {
                            onlyJSON = false;
                            origTree = json;
                        }
                        showResults();
                    }
                };
                fileReader.readAsText(file);
            });
        },
        toggleLoading = function() {
            $loading.toggleClass('hide');
            $results.toggleClass('hide');
        },
        getNetworkData = function(inputData) {
            var network = {
                "nodes": [],
                "links": []
            };

            var sourceNodes = {};
            // get children for root;
            inputData.forEach(function(node) {
                var newNode = Object.assign({}, node);
                sourceNodes[newNode.id] = newNode;
                newNode.hasParent = false;
            });

            var loadGroup = function(node) {
                var nodeGroup = node.group;
                var children = node.children || [];

                children.forEach(function(kid) {
                    var kidId = kid.id || kid;
                    var childNode = sourceNodes[kidId];
                    // First come, first served
                    if (childNode && !childNode.hasParent) {
                        childNode = Object.assign({}, childNode);
                        sourceNodes[kidId].hasParent = true;
                        childNode.group = nodeGroup + 1;
                        network.nodes.push(childNode);
                        loadGroup(childNode);
                        if (node.group) {
                            network.links.push({
                                source: childNode,
                                target: node,
                                value: 2
                            });
                        }
                    }
                });
            }

            loadGroup({ group: 0, children: inputData });

            return network;
        },
        showNetwork = function() {
            // create a new CodeFlower
            var networkData = getNetworkData(origTree || flatTreeArray);

            var width = $(document).width(), // svg width
                height = $(document).height(), // svg height
                dr = 4, // default point radius
                off = 15, // cluster hull offset
                expand = {}, // expanded clusters
                data, net, force, hullg, hull, linkg, link, nodeg, node, text, groupNode;

            var curve = d3.svg.line()
                .interpolate("cardinal-closed")
                .tension(.85);

            var fill = d3.scale.category20();

            function noop() {
                return false;
            }

            function nodeid(n) {
                return n.size ? "_g_" + n.group : n.name;
            }

            function linkid(l) {
                var u = nodeid(l.source),
                    v = nodeid(l.target);
                return u < v ? u + "|" + v : v + "|" + u;
            }

            function getGroup(n) {
                return n.group;
            }

            // constructs the network to visualize
            function network(data, prev, index, expand) {
                expand = expand || {};
                var gm = {}, // group map
                    nm = {}, // node map
                    lm = {}, // link map
                    gn = {}, // previous group nodes
                    gc = {}, // previous group centroids
                    nodes = [], // output nodes
                    links = []; // output links


                // process previous nodes for reuse or centroid calculation
                if (prev) {
                    prev.nodes.forEach(function(n) {
                        var i = index(n),
                            o;
                        if (n.size > 0) {
                            gn[i] = n;
                            n.size = 0;
                        } else {
                            o = gc[i] || (gc[i] = {
                                x: 0,
                                y: 0,
                                count: 0
                            });
                            o.x += n.x;
                            o.y += n.y;
                            o.count += 1;
                        }
                    });
                }

                // determine nodes
                for (var k = 0; k < data.nodes.length; ++k) {
                    var n = data.nodes[k],
                        i = index(n),
                        l = gm[i] || (gm[i] = gn[i]) || (gm[i] = {
                            group: i,
                            size: 0,
                            nodes: []
                        });

                    if (expand[i]) {
                        // the node should be directly visible
                        nm[n.name] = nodes.length;
                        nodes.push(n);
                        if (gn[i]) {
                            // place new nodes at cluster location (plus jitter)
                            n.x = gn[i].x + Math.random();
                            n.y = gn[i].y + Math.random();
                        }
                    } else {
                        // the node is part of a collapsed cluster
                        if (l.size == 0) {
                            // if new cluster, add to set and position at centroid of leaf nodes
                            nm[i] = nodes.length;
                            nodes.push(l);
                            if (gc[i]) {
                                l.x = gc[i].x / gc[i].count;
                                l.y = gc[i].y / gc[i].count;
                            }
                        }
                        l.nodes.push(n);
                    }
                    // always count group size as we also use it to tweak the force graph strengths/distances
                    l.size += 1;
                    n.group_data = l;
                }

                for (i in gm) {
                    gm[i].link_count = 0;
                }

                // determine links
                for (k = 0; k < data.links.length; ++k) {
                    var e = data.links[k],
                        u = index(e.source),
                        v = index(e.target);

                    if (u != v) {
                        gm[u].link_count++;
                        gm[v].link_count++;
                    }
                    u = expand[u] ? nm[e.source.name] : nm[u];
                    v = expand[v] ? nm[e.target.name] : nm[v];
                    var i = (u < v ? u + "|" + v : v + "|" + u),
                        l = lm[i] || (lm[i] = {
                            source: u,
                            target: v,
                            size: 0
                        });
                    l.size += 1;
                }
                for (i in lm) {
                    links.push(lm[i]);
                }

                return {
                    nodes: nodes,
                    links: links
                };
            }

            function convexHulls(nodes, index, offset) {
                var hulls = {};

                // create point sets
                for (var k = 0; k < nodes.length; ++k) {
                    var n = nodes[k];
                    if (n.size) continue;
                    var i = index(n),
                        l = hulls[i] || (hulls[i] = []);
                    l.push([n.x - offset, n.y - offset]);
                    l.push([n.x - offset, n.y + offset]);
                    l.push([n.x + offset, n.y - offset]);
                    l.push([n.x + offset, n.y + offset]);
                }

                // create convex hulls
                var hullset = [];
                for (i in hulls) {
                    hullset.push({
                        group: i,
                        path: d3.geom.hull(hulls[i])
                    });
                }

                return hullset;
            }

            function drawCluster(d) {
                return curve(d.path); // 0.8
            }


            function init() {
                if (force) force.stop();

                net = network(networkData, net, getGroup, expand);

                force = d3.layout.force()
                    .nodes(net.nodes)
                    .links(net.links)
                    .size([width, height])
                    .linkDistance(function(l, i) {
                        var n1 = l.source,
                            n2 = l.target;
                        return 30 +
                            Math.min(20 * Math.min((n1.size || (n1.group != n2.group ? n1.group_data.size : 0)),
                                    (n2.size || (n1.group != n2.group ? n2.group_data.size : 0))), -30 +
                                30 * Math.min((n1.link_count || (n1.group != n2.group ? n1.group_data.link_count : 0)),
                                    (n2.link_count || (n1.group != n2.group ? n2.group_data.link_count : 0))),
                                100);
                    })
                    .linkStrength(function(l, i) {
                        return 1;
                    })
                    .gravity(0.05) // gravity+charge tweaked to ensure good 'grouped' view (e.g. green group not smack between blue&orange, ...
                    .charge(-600) // ... charge is important to turn single-linked groups to the outside
                    .friction(0.5) // friction adjusted to get dampened display: less bouncy bouncy ball [Swedish Chef, anyone?]
                    .start();

                hullg.selectAll("path.hull").remove();
                hull = hullg.selectAll("path.hull")
                    .data(convexHulls(net.nodes, getGroup, off))
                    .enter().append("path")
                    .attr("class", "hull")
                    .attr("d", drawCluster)
                    .style("fill", function(d) {
                        return fill(d.group);
                    })
                    .on("click", function(d) {
                        console.log("hull click", d, arguments, this, expand[d.group]);
                        expand[d.group] = false;
                        init();
                    });

                link = linkg.selectAll("line.link").data(net.links, linkid);
                link.exit().remove();
                link.enter().append("line")
                    .attr("class", "link")
                    .attr("x1", function(d) {
                        return d.source.x;
                    })
                    .attr("y1", function(d) {
                        return d.source.y;
                    })
                    .attr("x2", function(d) {
                        return d.target.x;
                    })
                    .attr("y2", function(d) {
                        return d.target.y;
                    })
                    .style("stroke-width", "2px")
                    .style("stroke", function(d) {
                        return fill(d.group);
                    });

                groupNode = nodeg.selectAll("g.group").data(net.nodes, nodeid);
                groupNode.exit().remove();
                var nodeEnter = groupNode.enter().append("g")
                .attr("class", "group")
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

                nodeEnter.append("circle")
                    .attr("class", function(d) {
                        return "node" + (d.size ? "" : " leaf");
                    })
                    .attr("r", function(d) {
                        return d.size ? d.size + dr : dr + 1;
                    })
                    .style("stroke", function(d) {
                        return "#fff";
                    })
                    .style("stroke-width", "2px")
                    .style("fill", function(d) {
                        return fill(d.group);
                    })
                    .on("click", function(d) {
                        expand[d.group] = !expand[d.group];
                        init();
                    });

                nodeEnter.append('text')
                    .attr('text-anchor', 'middle')
                    .attr('dominant-baseline', 'central')
                    .style('font-size', '15px')
                    .text(function(d) {
                        return d.name || ("Group " + d.group);
                    })
                    .attr("x", 0)
                    .attr("y", function(d) {
                        var y = d.size || 0;
                        return y + dr + 5;
                    });

                groupNode.call(force.drag);

                force.on("tick", function() {
                    if (!hull.empty()) {
                        hull.data(convexHulls(net.nodes, getGroup, off))
                            .attr("d", drawCluster);
                    }

                    link.attr("x1", function(d) {
                            return d.source.x;
                        })
                        .attr("y1", function(d) {
                            return d.source.y;
                        })
                        .attr("x2", function(d) {
                            return d.target.x;
                        })
                        .attr("y2", function(d) {
                            return d.target.y;
                        });

                    groupNode.attr("transform", function(d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    });
                });
            }

            // TODO: Pan function, can be better implemented.
            var panTimer = 0;
            function pan(domNode, direction) {
                var speed = panSpeed,
                    scaleX,scaleY;

                if (panTimer) {
                    clearTimeout(panTimer);
                    var translateCoords = d3.transform(svgGroup.attr("transform"));
                    var translateX, translateY;
                    if (direction == 'left' || direction == 'right') {
                        translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                        translateY = translateCoords.translate[1];
                    } else if (direction == 'up' || direction == 'down') {
                        translateX = translateCoords.translate[0];
                        translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
                    }
                    scaleX = translateCoords.scale[0];
                    scaleY = translateCoords.scale[1];
                    scale = zoomListener.scale();
                    svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
                    d3.select(domNode).select('g.group').attr("transform", "translate(" + translateX + "," + translateY + ")");
                    zoomListener.scale(zoomListener.scale());
                    zoomListener.translate([translateX, translateY]);
                    panTimer = setTimeout(function() {
                        pan(domNode, speed, direction);
                    }, 50);
                }
            }

            // Define the zoom function for the zoomable tree
            function zoom() {
                svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            }

            // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
            var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

            function initiateDrag(d, domNode) {
                draggingNode = d;
                d3.select(domNode).attr('class', 'node activeDrag');
                dragStarted = null;
            }

            // --------------------------------------------------------
            $("#network-content").html("");
            var baseSvg = d3.select("#network-content").append("svg")
                .attr("class", "overlay")
                .attr("height", "3000px")
                .attr("width", "3000px")
                .call(zoomListener);

            var svgGroup = baseSvg.append("g").attr("class", "cover-graph");
            hullg = svgGroup.append("g").attr("class", "cover-hull");
            linkg = svgGroup.append("g").attr("class", "cover-link");
            nodeg = svgGroup.append("g").attr("class", "cover-node");

            svgGroup.attr("opacity", 1e-6)
                .transition()
                .duration(1000)
                .attr("opacity", 1);

            init();

            networkTab = true;
            var relCoords, panBoundary;
            // Define the drag listeners for drag/drop behaviour of nodes.
            var dragListener = d3.behavior.drag()
                .on("dragstart", function(d) {
                    if (d == root) {
                        return;
                    }
                    dragStarted = true;
                    nodes = tree.nodes(d);
                    d3.event.sourceEvent.stopPropagation();
                })
                .on("drag", function(d) {
                    if (d == root) {
                        return;
                    }
                    if (dragStarted) {
                        domNode = this;
                        initiateDrag(d, domNode);
                    }

                    // get coords of mouseEvent relative to svg container to allow for panning
                    relCoords = d3.mouse($('svg').get(0));
                    if (relCoords[0] < panBoundary) {
                        panTimer = true;
                        pan(this, 'left');
                    } else if (relCoords[0] > ($('svg').width() - panBoundary)) {

                        panTimer = true;
                        pan(this, 'right');
                    } else if (relCoords[1] < panBoundary) {
                        panTimer = true;
                        pan(this, 'up');
                    } else if (relCoords[1] > ($('svg').height() - panBoundary)) {
                        panTimer = true;
                        pan(this, 'down');
                    } else {
                        try {
                            clearTimeout(panTimer);
                        } catch (e) {

                        }
                    }

                    d.x += d3.event.dy;
                    d.y += d3.event.dx;
                    var node = d3.select(this);
                    node.attr("transform", "translate(" + d.y + "," + d.x + ")");
                }).on("dragend", function(d) {
                    if (d == root) {
                        return;
                    }
                    domNode = this;

                    endDrag();

                });

            function endDrag() {
                selectedNode = null;
                d3.select(domNode).attr('class', 'node');
                draggingNode = null;
            }
        },

        showInsight = function() {

        },
        showDND = function() {
            // Calculate total nodes, max label length
            var totalNodes = 0,
                maxLabelLength = 0,
                // variables for drag/drop
                selectedNode = null,
                draggingNode = null,
                // panning variables
                panSpeed = 200,
                panBoundary = 20, // Within 20px from edges will pan when dragging.
                // Misc. variables
                i = 0,
                duration = 750,
                root,
                domNode,
                dragStarted,
                nodes,
                links,
                nodePaths,
                nodesExit,
                relCoords,
                panTimer,
                // size of the diagram
                viewerWidth = $(document).width(),
                viewerHeight = $(document).height(),
                nodeWidth = 300,
                nodeHeight = 75,
                horizontalSeparationBetweenNodes = 16,
                verticalSeparationBetweenNodes = 128;

            var tree = d3.layout.tree()
                .nodeSize([nodeWidth + horizontalSeparationBetweenNodes, nodeHeight + verticalSeparationBetweenNodes])
                .separation(function(a, b) {
                    return a.parent == b.parent ? 2 : 2.5;
                });

            // define a d3 diagonal projection for use by the node paths later on.
            var diagonal = d3.svg.diagonal()
                .projection(function(d) {
                    return [d.y, d.x];
                });

            // A recursive helper function for performing some setup by walking through all nodes
            function visit(parent, visitFn, childrenFn) {
                if (!parent) return;
                visitFn(parent);
                var children = childrenFn(parent);
                if (children) {
                    var count = children.length;
                    for (var i = 0; i < count; i++) {
                        visit(children[i], visitFn, childrenFn);
                    }
                }
            }

            // Call visit function to establish maxLabelLength
            visit(jsonTree, function(d) {
                totalNodes++;
                if (!d.name) {
                    d.name = d.friendlyName || "";
                }
                var len = Math.min(d.name.length, 20);
                maxLabelLength = Math.max(len, maxLabelLength);
            }, function(d) {
                return d.children && d.children.length > 0 ? d.children : null;
            });
            // sort the tree according to the node names

            function sortTree() {
                tree.sort(function(a, b) {
                    return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
                });
            }
            // Sort the tree initially incase the JSON isn't in a sorted order.
            sortTree();

            // TODO: Pan function, can be better implemented.
            function pan(domNode, direction) {
                var speed = panSpeed,
                    scaleX,scaleY;

                if (panTimer) {
                    clearTimeout(panTimer);
                    var translateCoords = d3.transform(svgGroup.attr("transform"));
                    var translateX, translateY;
                    if (direction == 'left' || direction == 'right') {
                        translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                        translateY = translateCoords.translate[1];
                    } else if (direction == 'up' || direction == 'down') {
                        translateX = translateCoords.translate[0];
                        translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
                    }
                    scaleX = translateCoords.scale[0];
                    scaleY = translateCoords.scale[1];
                    scale = zoomListener.scale();
                    svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
                    d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX + "," + translateY + ")");
                    zoomListener.scale(zoomListener.scale());
                    zoomListener.translate([translateX, translateY]);
                    panTimer = setTimeout(function() {
                        pan(domNode, speed, direction);
                    }, 50);
                }
            }

            // Define the zoom function for the zoomable tree
            function zoom() {
                svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            }

            // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
            var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

            function initiateDrag(d, domNode) {
                draggingNode = d;
                d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
                d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
                d3.select(domNode).attr('class', 'node activeDrag');

                childGroup.selectAll("g.node").sort(function(a, b) { // select the parent and sort the path's
                    if (a.id != draggingNode.id) return 1; // a is not the hovered element, send "a" to the back
                    else return -1; // a is the hovered element, bring "a" to the front
                });
                // if nodes has children, remove the links and nodes
                if (nodes.length > 1) {
                    // remove link paths
                    links = tree.links(nodes);
                    nodePaths = childGroup.selectAll("path.link")
                        .data(links, function(d) {
                            return d.target.id;
                        }).remove();
                    // remove child nodes
                    nodesExit = childGroup.selectAll("g.node")
                        .data(nodes, function(d) {
                            return d.id;
                        }).filter(function(d, i) {
                            if (d.id == draggingNode.id) {
                                return false;
                            }
                            return true;
                        }).remove();
                }

                // remove parent link
                var parentLink = tree.links(tree.nodes(draggingNode.parent));
                childGroup.selectAll('path.link').filter(function(d, i) {
                    if (d.target.id == draggingNode.id) {
                        return true;
                    }
                    return false;
                }).remove();

                dragStarted = null;
            }

            // define the baseSvg, attaching a class for styling and the zoomListener
            var baseSvg = d3.select("#tree-content").html("").append("svg")
                .attr("class", "overlay")
                .attr("height", "100%")
                .attr("width", "100%")
                .call(zoomListener);

            // Define the drag listeners for drag/drop behaviour of nodes.
            var dragListener = d3.behavior.drag()
                .on("dragstart", function(d) {
                    if (d == root) {
                        return;
                    }
                    dragStarted = true;
                    nodes = tree.nodes(d);
                    d3.event.sourceEvent.stopPropagation();
                })
                .on("drag", function(d) {
                    if (d == root) {
                        return;
                    }
                    if (dragStarted) {
                        domNode = this;
                        initiateDrag(d, domNode);
                    }

                    // get coords of mouseEvent relative to svg container to allow for panning
                    relCoords = d3.mouse($('svg').get(0));
                    if (relCoords[0] < panBoundary) {
                        panTimer = true;
                        pan(this, 'left');
                    } else if (relCoords[0] > ($('svg').width() - panBoundary)) {

                        panTimer = true;
                        pan(this, 'right');
                    } else if (relCoords[1] < panBoundary) {
                        panTimer = true;
                        pan(this, 'up');
                    } else if (relCoords[1] > ($('svg').height() - panBoundary)) {
                        panTimer = true;
                        pan(this, 'down');
                    } else {
                        try {
                            clearTimeout(panTimer);
                        } catch (e) {

                        }
                    }

                    d.x0 += d3.event.dy;
                    d.y0 += d3.event.dx;
                    var node = d3.select(this);
                    node.attr("transform", "translate(" + d.y0 + "," + d.x0 + ")");
                    updateTempConnector();
                }).on("dragend", function(d) {
                    if (d == root) {
                        return;
                    }
                    domNode = this;
                    if (selectedNode) {
                        // now remove the element from the parent, and insert it into the new elements children
                        var index = draggingNode.parent.children.indexOf(draggingNode);
                        if (index > -1) {
                            draggingNode.parent.children.splice(index, 1);
                        }
                        if (typeof selectedNode.children !== 'undefined' || typeof selectedNode._children !== 'undefined') {
                            if (typeof selectedNode.children !== 'undefined') {
                                selectedNode.children.push(draggingNode);
                            } else {
                                selectedNode._children.push(draggingNode);
                            }
                        } else {
                            selectedNode.children = [];
                            selectedNode.children.push(draggingNode);
                        }
                        // Make sure that the node being added to is expanded so user can see added node is correctly moved
                        expand(selectedNode);
                        sortTree();
                        endDrag();
                    } else {
                        endDrag();
                    }
                });

            function endDrag() {
                selectedNode = null;
                d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
                d3.select(domNode).attr('class', 'node');
                // now restore the mouseover event or we won't be able to drag a 2nd time
                d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
                updateTempConnector();
                if (draggingNode !== null) {
                    update(root);
                    draggingNode = null;
                }
            }

            // Helper functions for collapsing and expanding nodes.
            function collapse(d) {
                if (d.children) {
                    d._children = d.children;
                    d._children.forEach(collapse);
                    d.children = null;
                }
            }

            function expand(d) {
                if (d._children) {
                    d.children = d._children;
                    d.children.forEach(expand);
                    d._children = null;
                }
            }

            var overCircle = function(d) {
                selectedNode = d;
                updateTempConnector();
            };

            var outCircle = function(d) {
                selectedNode = null;
                updateTempConnector();
            };

            // Function to update the temporary connector indicating dragging affiliation
            var updateTempConnector = function() {
                var data = [];
                if (draggingNode !== null && selectedNode !== null) {
                    // have to flip the source coordinates since we did this for the existing connectors on the original tree
                    data = [{
                        source: {
                            x: selectedNode.y0,
                            y: selectedNode.x0
                        },
                        target: {
                            x: draggingNode.y0,
                            y: draggingNode.x0
                        }
                    }];
                }
                var link = childGroup.selectAll(".templink").data(data);

                link.enter().append("path")
                    .attr("class", "templink")
                    .attr("d", d3.svg.diagonal())
                    .attr('pointer-events', 'none');

                link.attr("d", d3.svg.diagonal());

                link.exit().remove();
            };

            // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.
            function centerNode(source) {
                var scale = zoomListener.scale(),
                    x = -source.y0,
                    y = -source.x0;

                if (isRotate) {
                    scale = 0.5;
                    x = 700;
                    y = 100;
                }

                d3.select('g').transition()
                    .duration(duration)
                    .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
                zoomListener.scale(scale);
                zoomListener.translate([x, y]);
            }

            // Toggle children function
            function toggleChildren(d) {
                if (d.children) {
                    d._children = d.children;
                    d.children = null;
                } else if (d._children) {
                    d.children = d._children;
                    d._children = null;
                }
                return d;
            }

            // Toggle children on click.

            function click(d) {
                if (d3.event.defaultPrevented) return;
                d = toggleChildren(d);
                update(d);
            }

            function getExtraLinks(nodes) {
                var connect = [],
                    nodeIds = {},
                    parents = [];

                for (var i = 0; i < nodes.length; ++i) {
                    nodeIds[nodes[i].id] = nodes[i];
                }

                for (var i = 0; i < nodes.length; ++i) {
                    parents = nodes[i].parents;
                    if (parents && parents.length) {
                        for (var p = 0; p < parents.length; ++p) {
                            var parent = nodeIds[parents[p]];
                            parent || (parent = nodeIds.root)
                            connect.push({
                                source: parent,
                                target: nodes[i]
                            });
                        }
                    }
                }
                return connect;
            }

            function update(source) {
                // Compute the new height, function counts total children of root node and sets tree height accordingly.
                // This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
                // This makes the layout more consistent.
                var levelWidth = [1];
                var childCount = function(level, n) {
                    if (n.children && n.children.length > 0) {
                        if (levelWidth.length <= level + 1) levelWidth.push(0);

                        levelWidth[level + 1] += n.children.length;
                        n.children.forEach(function(d) {
                            childCount(level + 1, d);
                        });
                    }
                };
                childCount(0, root);
                var newHeight = d3.max(levelWidth) * 45; // 25 pixels per line
                tree = tree.size([newHeight, viewerWidth]);

                // Compute the new tree layout.
                nodes = tree.nodes(root).reverse();
                nodes.forEach(function(node) {
                    node.y = (node.depth * 110); // 100px per level.
                });
                links = getExtraLinks(nodes);

                // Set widths between levels based on maxLabelLength.
                nodes.forEach(function(d) {
                    d.y = (d.depth * (maxLabelLength * 10)); //maxLabelLength * 10px
                    // alternatively to keep a fixed scale one can set a fixed depth per level
                    // Normalize for fixed-depth by commenting out below line
                    // d.y = (d.depth * 500); //500px per level.
                });

                // Update the nodes…
                var node = childGroup.selectAll("g.node")
                    .data(nodes, function(d) {
                        return d.id || (d.id = ++i);
                    });

                // Enter any new nodes at the parent's previous position.
                var nodeEnter = node.enter().append("g")
                    .call(dragListener)
                    .attr("class", "node")
                    .attr("transform", function(d) {
                        return "translate(" + source.y0 + "," + source.x0 + ")";
                    })
                    .on('click', click)
                    .on('mouseout', hideToolTip)
                    .on('mouseover', showToolTip);

                nodeEnter.append("path")
                    .attr('class', 'nodePath')
                    .attr("d", function(d) {
                         var t = d.type? d.type.substr(0,1):DEFAULT;
                         return shape[t]?shape[t]:shape[DEFAULT];
                     })
                    .style("stroke", function(d) {
                        return d.status ? status_colors[d.status] : status_colors[DEFAULT];
                    })
                    .style("fill", function(d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });
                nodeEnter.append('text')
                    .attr('class', function(d) {
                        return 'icon ' + d.type;
                    })
                    .attr('text-anchor', 'middle')
                    .attr('dominant-baseline', 'central')
                    .style('font-family', 'FontAwesome')
                    .style('font-size', '15px')
                    .text(function(d) {
                        return '\uf007';
                    })
                    .attr("x", "-5");

                nodeEnter.append("text")
                    .attr("x", function(d) {
                        return d.children || d._children ? -10 : 10;
                    })
                    .attr("dy", ".35em")
                    .attr('class', 'nodeText')
                    .attr("text-anchor", function(d) {
                        return d.children || d._children ? "end" : "start";
                    })
                    .text(function(d) {
                        return d.name.substring(0, 10);
                    })
                    .style("fill-opacity", 0);

                // phantom node to give us mouseover in a radius around it
                nodeEnter.append("circle")
                    .attr('class', 'ghostCircle')
                    .attr("r", 30)
                    .attr("opacity", 0.2) // change this to zero to hide the target area
                    .style("fill", "red")
                    .attr('pointer-events', 'mouseover')
                    .on("mouseover", function(node) {
                        overCircle(node);
                    })
                    .on("mouseout", function(node) {
                        outCircle(node);
                    });

                // Update the text to reflect whether node has children or not.
                node.select('text')
                    .attr("x", function(d) {
                        return d.children || d._children ? -10 : 10;
                    })
                    .attr("text-anchor", function(d) {
                        return d.children || d._children ? "end" : "start";
                    })
                    .text(function(d) {
                        return d.name.substring(0, 10);
                    });

                // Change the circle fill depending on whether it has children and is collapsed
                node.select("path.nodePath")
                    .attr("r", 10)
                    .style("fill", function(d) {
                        // return d.status ? status_colors[d.status] : status_colors[DEFAULT];
                        return d._children ? "lightsteelblue" : "#fff";
                    });

                // Transition nodes to their new position.
                var nodeUpdate = node.transition()
                    .duration(duration)
                    .attr("transform", function(d) {
                        return "translate(" + d.y + "," + d.x + ")";
                    });

                // Fade the text in
                nodeUpdate.select("text")
                    .style("fill-opacity", 1);

                // Transition exiting nodes to the parent's new position.
                var nodeExit = node.exit().transition()
                    .duration(duration)
                    .attr("transform", function(d) {
                        return "translate(" + source.y + "," + source.x + ")";
                    })
                    .remove();

                nodeExit.select("path.nodePath")
                    .attr("size", 0);

                nodeExit.select("text")
                    .style("fill-opacity", 0);

                // Update the links…
                var link = childGroup.selectAll("path.link")
                    .data(links);

                // Enter any new links at the parent's previous position.
                link.enter().insert("path", "g")
                    .attr("class", "link")
                    .attr("d", function(d) {
                        var o = {
                            x: source.x0,
                            y: source.y0
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    })
                    .attr("stroke", function(d) {
                        return status_colors[d.target.status] ? status_colors[d.target.status] : status_colors[DEFAULT];
                    });

                // Transition links to their new position.
                link.transition()
                    .duration(duration)
                    .attr("d", diagonal);

                // Transition exiting nodes to the parent's new position.
                link.exit().transition()
                    .duration(duration)
                    .attr("d", function(d) {
                        var o = {
                            x: source.x,
                            y: source.y
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    })
                    .remove();

                // Stash the old positions for transition.
                var maxX = $("#scroll-box").width();
                var maxY = $("#scroll-box").height();
                nodes.forEach(function(d) {
                    d.x0 = d.x;
                    d.y0 = d.y;
                    if (d.x > maxY) {
                        maxY = d.x;
                    }
                    if (d.y > maxX) {
                        maxX = d.y;
                    }
                });
                console.log(maxY, maxX);
                baseSvg
                .attr("width", maxX + "px")
                .attr("height", maxY + "px");
                $("#scroll-box .scroll-content").width(maxX).height(maxY);
            }

            // Append a group which holds all nodes and which the zoom Listener can act upon.
            var svgGroup = baseSvg.append("g").attr("class", "pan-cover");
            var childGroup = svgGroup.append("g").attr("class", "child-group");

            // Define the root
            root = jsonTree;
            root.x0 = viewerHeight / 2;
            root.y0 = 0;
            var isRotate = false;
            var rotateButton = $("#rotate-button").click(function() {
                isRotate = !!!isRotate;
                if (isRotate) {
                    childGroup.attr("transform", "rotate(90)");
                } else {
                    childGroup.attr("transform", "rotate(0)");
                }
                update(root);
                centerNode(root);
            });

            // Layout the tree initially and center on the root node.
            update(root);
            //centerNode(root);
        },
        showRadial = function() {
            hideToolTip();
            // Calculate total nodes, max label length
            var totalNodes = 0,
                maxLabelLength = 0,
                // variables for drag/drop
                selectedNode = null,
                draggingNode = null,
                // panning variables
                panSpeed = 200,
                panBoundary = 20, // Within 20px from edges will pan when dragging.
                // Misc. variables
                i = 0,
                duration = 750,
                root,
                firstTime = true,
                domNode,
                dragStarted,
                nodes,
                links,
                nodePaths,
                nodesExit,
                relCoords,
                panTimer,
                // size of the diagram
                viewerWidth = $(document).width(),
                viewerHeight = $(document).height(),
                nodeWidth = 300,
                nodeHeight = 75,
                horizontalSeparationBetweenNodes = 16,
                verticalSeparationBetweenNodes = 128;
            // Calculate total nodes, max label length

            var diameter = 800;

            var tree = d3.layout.tree().size([360, diameter / 2 - 120])
                .separation(function(a, b) {
                    return (a.parent == b.parent ? 1 : 10) / a.depth;
                });

            // define a d3 diagonal projection for use by the node paths later on.
            var diagonal = d3.svg.diagonal.radial()
                .projection(function(d) {
                    return [d.y, d.x / 180 * Math.PI];
                });

            // Define the root
            root = jsonTree;
            root.x0 = height / 2;
            root.y0 = 0;

            // A recursive helper function for performing some setup by walking through all nodes

            function visit(parent, visitFn, childrenFn) {
                if (!parent) return;
                visitFn(parent);
                var children = childrenFn(parent);
                if (children) {
                    var count = children.length;
                    for (var i = 0; i < count; i++) {
                        visit(children[i], visitFn, childrenFn);
                    }
                }
            }

            // Call visit function to establish maxLabelLength
            visit(jsonTree, function(d) {
                totalNodes++;
                maxLabelLength = Math.max(d.name.length, maxLabelLength);
            }, function(d) {
                return d.children && d.children.length > 0 ? d.children : null;
            });


            // sort the tree according to the node names
            function sortTree() {
                tree.sort(function(a, b) {
                    return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
                });
            }

            // Sort the tree initially incase the JSON isn't in a sorted order.
            sortTree();

            // TODO: Pan function, can be better implemented.
            function pan(domNode, direction) {
                var speed = panSpeed;
                if (panTimer) {
                    clearTimeout(panTimer);
                    var translateX, translateY;
                    translateCoords = d3.transform(svgGroup.attr("transform"));
                    if (direction == 'left' || direction == 'right') {
                        translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                        translateY = translateCoords.translate[1];
                    } else if (direction == 'up' || direction == 'down') {
                        translateX = translateCoords.translate[0];
                        translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
                    }
                    scaleX = translateCoords.scale[0];
                    scaleY = translateCoords.scale[1];
                    scale = zoomListener.scale();
                    svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
                    d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX + "," + translateY + ")");
                    zoomListener.scale(zoomListener.scale());
                    zoomListener.translate([translateX, translateY]);
                    panTimer = setTimeout(function() {
                        pan(domNode, speed, direction);
                    }, 50);
                }
            }

            // Define the zoom function for the zoomable tree
            function zoom() {
                svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            }

            // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
            var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

            function initiateDrag(d, domNode) {
                draggingNode = d;
                d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
                d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
                d3.select(domNode).attr('class', 'node activeDrag');

                svgGroup.selectAll("g.node").sort(function(a, b) { // select the parent and sort the path's
                    if (a.id != draggingNode.id) return 1; // a is not the hovered element, send "a" to the back
                    else return -1; // a is the hovered element, bring "a" to the front
                });
                // if nodes has children, remove the links and nodes
                if (nodes.length > 1) {
                    // remove link paths
                    links = tree.links(nodes);
                    nodePaths = svgGroup.selectAll("path.link")
                        .data(links, function(d) {
                            return d.target.id;
                        }).remove();
                    // remove child nodes
                    nodesExit = svgGroup.selectAll("g.node")
                        .data(nodes, function(d) {
                            return d.id;
                        }).filter(function(d, i) {
                            if (d.id == draggingNode.id) {
                                return false;
                            }
                            return true;
                        }).remove();
                }

                // remove parent link
                parentLink = tree.links(tree.nodes(draggingNode.parent));
                svgGroup.selectAll('path.link').filter(function(d, i) {
                    if (d.target.id == draggingNode.id) {
                        return true;
                    }
                    return false;
                }).remove();

                dragStarted = null;
            }

            // define the baseSvg, attaching a class for styling and the zoomListener
            var baseSvg = d3.select("#radial-content").append("svg")
                .attr("class", "overlay")
                .attr("height", "3000px")
                .attr("width", "3000px")
                .call(zoomListener);

            // Define the drag listeners for drag/drop behaviour of nodes.
            var dragListener = d3.behavior.drag()
                .on("dragstart", function(d) {
                    if (d == root) {
                        return;
                    }
                    dragStarted = true;
                    nodes = tree.nodes(d);
                    d3.event.sourceEvent.stopPropagation();
                    // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
                })
                .on("drag", function(d) {
                    if (d == root) {
                        return;
                    }
                    if (dragStarted) {
                        domNode = this;
                        initiateDrag(d, domNode);
                    }

                    // get coords of mouseEvent relative to svg container to allow for panning
                    relCoords = d3.mouse($('svg').get(0));
                    if (relCoords[0] < panBoundary) {
                        panTimer = true;
                        pan(this, 'left');
                    } else if (relCoords[0] > ($('svg').width() - panBoundary)) {

                        panTimer = true;
                        pan(this, 'right');
                    } else if (relCoords[1] < panBoundary) {
                        panTimer = true;
                        pan(this, 'up');
                    } else if (relCoords[1] > ($('svg').height() - panBoundary)) {
                        panTimer = true;
                        pan(this, 'down');
                    } else {
                        try {
                            clearTimeout(panTimer);
                        } catch (e) {

                        }
                    }

                    d.x0 += d3.event.dy;
                    d.y0 += d3.event.dx;
                    var node = d3.select(this);
                    node.attr("transform", "translate(" + d.y0 + "," + (d.x - 90) + ")");
                    updateTempConnector();
                })
                .on("dragend", function(d) {
                    if (d == root) {
                        return;
                    }
                    domNode = this;
                    if (selectedNode) {
                        // now remove the element from the parent, and insert it into the new elements children
                        var index = draggingNode.parent.children.indexOf(draggingNode);
                        if (index > -1) {
                            draggingNode.parent.children.splice(index, 1);
                        }
                        if (typeof selectedNode.children !== 'undefined' || typeof selectedNode._children !== 'undefined') {
                            if (typeof selectedNode.children !== 'undefined') {
                                selectedNode.children.push(draggingNode);
                            } else {
                                selectedNode._children.push(draggingNode);
                            }
                        } else {
                            selectedNode.children = [];
                            selectedNode.children.push(draggingNode);
                        }
                        // Make sure that the node being added to is expanded so user can see added node is correctly moved
                        expand(selectedNode);
                        sortTree();
                        endDrag();
                    } else {
                        endDrag();
                    }
                });

            function endDrag() {
                selectedNode = null;
                d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
                d3.select(domNode).attr('class', 'node');
                // now restore the mouseover event or we won't be able to drag a 2nd time
                d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
                updateTempConnector();
                if (draggingNode !== null) {
                    update(root);
                    //centerNode(draggingNode);
                    draggingNode = null;
                }
            }

            // Helper functions for collapsing and expanding nodes.
            function collapse(d) {
                if (d.children) {
                    d._children = d.children;
                    d._children.forEach(collapse);
                    d.children = null;
                }
            }

            function expand(d) {
                if (d._children) {
                    d.children = d._children;
                    d.children.forEach(expand);
                    d._children = null;
                }
            }

            var overCircle = function(d) {
                selectedNode = d;
                updateTempConnector();
            };
            var outCircle = function(d) {
                selectedNode = null;
                updateTempConnector();
            };

            // Function to update the temporary connector indicating dragging affiliation
            var updateTempConnector = function() {
                var data = [];
                if (draggingNode !== null && selectedNode !== null) {
                    // have to flip the source coordinates since we did this for the existing connectors on the original tree
                    data = [{
                        source: {
                            x: selectedNode.y0,
                            y: selectedNode.x0
                        },
                        target: {
                            x: draggingNode.y0,
                            y: draggingNode.x0
                        }
                    }];
                }
                var link = svgGroup.selectAll(".templink").data(data);

                link.enter().append("path")
                    .attr("class", "templink")
                    .attr("d", d3.svg.diagonal.radial())
                    .attr('pointer-events', 'none');

                link.attr("d", d3.svg.diagonal.radial());

                link.exit().remove();
            };

            // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.
            function centerNode(source) {
                scale = zoomListener.scale();
                x = -source.y0;
                y = -source.x0;
                x = x * scale + width / 2;
                y = y * scale + height / 2;
                d3.select('g').transition()
                    .duration(duration)
                    .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
                zoomListener.scale(scale);
                zoomListener.translate([x, y]);
            }

            // Toggle children function
            function toggleChildren(d) {
                if (d.children) {
                    d._children = d.children;
                    d.children = null;
                } else if (d._children) {
                    d.children = d._children;
                    d._children = null;
                }
                return d;
            }

            // Toggle children on click.
            function click(d) {
                if (d3.event.defaultPrevented) return; // click suppressed
                d = toggleChildren(d);
                update(d);
            }

            function update(source) {

                var levelWidth = [1];
                var childCount = function(level, n) {
                    if (n.children && n.children.length > 0) {
                        if (levelWidth.length <= level + 1) levelWidth.push(0);

                        levelWidth[level + 1] += n.children.length;
                        n.children.forEach(function(d) {
                            childCount(level + 1, d);
                        });
                    }
                };
                childCount(0, root);

                // Compute the new tree layout.
                var nodes = tree.nodes(root); //.reverse(),
                links = tree.links(nodes);


                nodes.forEach(function(d) {
                    d.y = d.depth * 80;
                });

                // Update the nodes…
                var node = svgGroup.selectAll("g.node")
                    .data(nodes, function(d) {
                        return d.id || (d.id = ++i);
                    });

                // Enter any new nodes at the parent's previous position.
                var nodeEnter = node.enter().append("g")
                    .call(dragListener)
                    .attr("class", "node")
                    .on('click', click)


                nodeEnter.append("circle")
                    .attr('class', 'nodeCircle')
                    .attr("r", 1e-6)
                    .style("fill", function(d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });

                nodeEnter.append("text")
                    .text(function(d) {
                        return d.name;
                    })
                    .style("font", "8px serif")
                    .style("opacity", 0.9)
                    .style("fill-opacity", 0);

                // phantom node to give us mouseover in a radius around it
                nodeEnter.append("circle")
                    .attr('class', 'ghostCircle')
                    .attr("r", 30)
                    .attr("opacity", 0.2) // change this to zero to hide the target area
                    .style("fill", "red")
                    .attr('pointer-events', 'mouseover')
                    .on("mouseover", function(node) {
                        overCircle(node);
                    })
                    .on("mouseout", function(node) {
                        outCircle(node);
                    });

                // Change the circle fill depending on whether it has children and is collapsed
                node.select("circle.nodeCircle")
                    .attr("r", 4.5)
                    .style("stroke", function(d) {
                        return d.status ? status_colors[d.status] : status_colors[DEFAULT];
                    })
                    .style("fill", function(d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });


                var nodeUpdate = node.transition()
                    .duration(duration)
                    .attr("transform", function(d) {
                        return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")";
                    });

                nodeUpdate.select("circle")
                    .attr("r", 4.5)
                    .style("fill", function(d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });


                nodeUpdate.select("text")
                    .style("fill-opacity", 1)
                    .attr("dy", ".35em")
                    .attr("text-anchor", function(d) {
                        return d.x < 180 ? "start" : "end";
                    })
                    .attr("transform", function(d) {
                        return d.x < 180 ? "translate(8)" : "rotate(180)translate(-8)";
                    })

                // Transition exiting nodes to the parent's new position.
                var nodeExit = node.exit().transition()
                    .duration(duration)
                    .attr("transform", function(d) {
                        return "translate(" + source.y + "," + source.x + ")";
                    })
                    .remove();

                nodeExit.select("circle")
                    .attr("r", 0);

                nodeExit.select("text")
                    .style("fill-opacity", 0);

                // Update the links…
                var link = svgGroup.selectAll("path.link")
                    .data(links, function(d) {
                        return d.target.id;
                    });

                // Enter any new links at the parent's previous position.
                link.enter().insert("path", "g")
                    .attr("class", "link")
                    .attr("d", function(d) {
                        var o = {
                            x: source.x0,
                            y: source.y0
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    })
                    .style("stroke-width", "2px")
                    .style("stroke", function(d) {
                        return d.target.status ? status_colors[d.target.status] : status_colors[DEFAULT];
                    });

                // Transition links to their new position.
                link.transition()
                    .duration(duration)
                    .attr("d", diagonal);

                // Transition exiting nodes to the parent's new position.
                link.exit().transition()
                    .duration(duration)
                    .attr("d", function(d) {
                        var o = {
                            x: source.x,
                            y: source.y
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    })
                    .remove();

                // Stash the old positions for transition.
                nodes.forEach(function(d) {
                    d.x0 = d.x;
                    d.y0 = d.y;
                });
            }

            // Append a group which holds all nodes and which the zoom Listener can act upon.
            var svgGroup = baseSvg.append("g").attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

            // Collapse all children of roots children before rendering.
            root.children.forEach(function(child) {
                collapse(child);
            });

            // Layout the tree initially and center on the root node.
            update(root);
            //d3.select(self.frameElement).style("height", width);
        },
        showToolTip = function(node) {
            $('#tooltip').removeClass('hide');
            $('#tooltip').show(function() {
                var data = [],
                    val, key,
                    n = _.find(flatTreeArray, {
                        id: node.id
                    });
                if (!n) {
                    hideToolTip();
                    return;
                }
                for (key in dataKeys) {
                    if (!node[key] && !n[key]) continue;
                    if (n[key] && (n[key] instanceof Array)) {
                        val = n[key].length
                    } else {
                        if (node[key]) {
                            val = node[key];
                        } else {
                            val = n[key];
                        }
                    }

                    data.push({
                        key: dataKeys[key],
                        value: val
                    });
                }

                $('#tooltip-table').bootstrapTable("destroy");
                $('#tooltip-table').bootstrapTable({ data: data });
            });
        },
        hideToolTip = function(event) {
            $('#tooltip').hide();
        },
        isVertical = true,
        drawTree = function(jsonData, textSearch) {
            var activeTab = d3.select(".tab-content .active");
            var svgSize = activeTab.node().getBoundingClientRect();
            var padding = 48;

            var minX = 9999;
            var minY = 9999;
            var maxX = -9999;
            var maxY = -9999;
            var contentWidth = 0;
            var contentHeight = 0;
            var svgWidth = svgSize.width;
            var svgHeight = svgSize.height;
            var showAllTree = showFullTree;
            textSearch = textSearch.toLowerCase();

            function zoom() {
                svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            }

            var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

            var baseSvg = activeTab
                            .html("")
                            .append("svg")
                            .attr("width", "100%")
                            .attr("height", "100%")
                            .call(zoomListener);

            var svgGroup = baseSvg
                            .append("g")
                            .attr("class", "pan-cover")
                            .attr("transform", "translate(" + padding + "," + padding + ")");

            var dragGroup = svgGroup
                            .append("g")
                            .attr("class", "group drag-group");

            var pathGroup = svgGroup
                            .append("g")
                            .attr("class", "group path-group");

            var nodeGroup = svgGroup
                            .append("g")
                            .attr("class", "group node-group");

            // Define the drag listeners for drag/drop behaviour of nodes.
            var startDragPos = {X: 0, Y: 0}, dragStarted, draggingNode, dragScale = 1;

            var dragListener = d3.behavior.drag()
            .on("dragstart", function(d) {
                var event = d3.event.sourceEvent;
                event.stopPropagation();
                if (d.id == rootCluster.id) {
                    return;
                }

                dragScale = zoomListener.scale();
                dragStarted = true;
                startDragPos.X = event.pageX;
                startDragPos.Y = event.pageY;

                draggingNode = this.parentNode;

                draggingNode.originalPosX = d.posX;
                draggingNode.originalPosY = d.posY;

                cloneDraggingNode(d); // virtual dragged node;
                dragGroup.attr("transform", "translate(" + 0 + "," + 0 + ")");
            })
            .on("drag", function(d) {
                var event = d3.event.sourceEvent;
                if (d.id == rootCluster.id) {
                    return;
                }
                dragStarted = null;

                var dx = (event.pageX - startDragPos.X) / dragScale;
                var dy = (event.pageY - startDragPos.Y) / dragScale;
                d3.select(draggingNode).style("pointer-events", 'none');
                dragGroup.attr("transform", "translate(" + dx + "," + dy + ")");
            })
            .on("dragend", function(d) {
                var event = d3.event.sourceEvent;
                if (d.id == rootCluster.id) {
                    return;
                }

                if (dragStarted == null) {
                    // snap with cloest node;
                    var dx = (event.pageX - startDragPos.X) / dragScale;
                    var dy = (event.pageY - startDragPos.Y) / dragScale;
                    if (!isVertical) {
                        translateNode(d, dx, dy);
                    } else {
                        translateNode(d, dy, -dx);
                    }

                    /*
                    var closestNode = findCloseNode(d);
                    if (closestNode) {
                        // remove from all old parents;
                        for (var i in sourceNodes) {
                            var children = sourceNodes[i].children.filter(function(item) {
                                return item.id != d.id;
                            });
                            sourceNodes[i].children = children;
                        }
                        closestNode.children.push(d);
                    }
                    */

                    // rerender graph;
                    renderNode();
                }

                d3.select(draggingNode).style("pointer-events", "auto");

                // remove fake dragging nodes;
                dragGroup.selectAll("g.node").data([]).exit().remove();
                dragGroup.selectAll("path.connect").data([]).exit().remove();
            });

            function translateNode(node, transX, transY) {
                node.posX += transX;
                node.posY += transY;
                if (usePosCache) {
                    if (node.x != null) {
                        node.x += transX;
                    }
                    if (node.y != null) {
                        node.y += transY;
                    }
                } else {
                    node.x = node.posX;
                    node.y = node.posY;
                }
                node.dragging = true;
                node.children.forEach(function(item) {
                    translateNode(item, transX, transY);
                });
            }

            // find the closest node via distance;
            function findCloseNode(source) {
                var closestNode = null;
                var distance = 0;
                var node;
                var dx;
                var dy;
                var min = Number.MAX_VALUE;

                for (var i in sourceNodes) {
                    node = sourceNodes[i];
                    if (node.dragging == true) {
                        delete node.dragging;
                    } else {
                        dx = node.posX - source.posX;
                        dy = node.posY - source.posY;
                        distance = dx * dx + dy * dy;
                        if (min > distance) {
                            min = distance;
                            closestNode = node;
                        }
                    }
                }

                return closestNode;
            }

            function centerNode(source) {
                var scale = zoomListener.scale();
                var x = 0;
                var y = 0;
                if (!isVertical) {
                    scale = svgWidth / contentWidth;
                    scale = Math.max(scale, 0.3);
                    scale = Math.min(scale, 1);
                    y = svgHeight / 2;
                    x = Math.max(48, (svgWidth - scale * contentWidth) / 2);
                } else {
                    scale = svgHeight / contentHeight;
                    scale = Math.max(scale, 0.3);
                    scale = Math.min(scale, 1);
                    x = svgWidth / 2;
                    y = Math.max(48, (svgHeight - scale * contentHeight) / 2);
                }

                svgGroup.transition().attr({
                    "transform": "translate(" + x + "," + y + ")scale(" + scale + ")"
                });

                zoomListener.scale(scale);
                zoomListener.translate([x, y]);
            }

            var sourceNodes = {}; // node dictionary;
            var rootCluster = {
                children: [],
                name: "Root",
                size: 0,
                id: Date.now() // random id for dragdrop
            };

            sourceNodes[rootCluster.id] = rootCluster;

            // every node holds all child name of it;
            function getChildName(source) {
                var childName = [source.name];
                source.children.forEach(function(item) {
                    var child = sourceNodes[item.toString()] || item;
                    if (child && !child.hasChildName) {
                        // don't allow multi parents;
                        child.hasChildName = true;
                        childName = childName.concat(getChildName(child));
                    }
                });
                source.childName = childName;
                return childName;
            }

            if (jsonData.name === "Root") {
                rootCluster = jsonData;
                function restoreChild(node) {
                    node.children.forEach(function(item) {
                        sourceNodes[item.id] = item;
                        item.x = item.posX;
                        item.y = item.posY;
                        restoreChild(item);
                    });
                }
                restoreChild(rootCluster);
                rootCluster.x = rootCluster.posX;
                rootCluster.y = rootCluster.posY;
            } else {
                jsonData.forEach(function(node) {
                    if (!node.name) {
                        node.name = node.friendlyName;
                    }

                    var newNode = Object.assign({}, node);
                    // add first group to root;
                    if (!newNode.parents || !newNode.parents.length) {
                        rootCluster.children.push(newNode.id);
                    }
                    // create dictionary for quick access;
                    delete newNode.parents;
                    // assign node id;
                    if (!newNode.id) {
                        newNode.id = Date.now();
                    }
                    sourceNodes[newNode.id] = newNode;
                });

                getChildName(rootCluster);
            }


            function estimateSize(source) {
                var children = [];
                var size = 0;

                if (source.children) {
                    source.children.forEach(function(item) {
                        var child = sourceNodes[item.toString()] || item;
                        if (child) {
                            var childName = child.childName.join("::").toLowerCase();
                            if (showAllTree || !textSearch || childName.indexOf(textSearch) > -1) {
                                // allow multi parents;
                                // children.push(child);
                                if (!child.parentId) {
                                    children.push(child); // don't allow multi parents;
                                    child.parentId = source.id; // main parent node, use for set position;
                                    size += estimateSize(child);
                                }
                            }
                        }
                    });
                    source.children = children;
                }

                source.size = size ? size : 20;
                return source.size;
            }

            function createConnect(source, nodes, links) {
                if (!source) return false;
                var children = source.children;
                var offset = 0;
                var size = source.size;
                var startX = source.posX + 200;
                var startY = source.posY - size / 2;

                if (children && children.length) {

                    children.forEach(function(child) {
                        // get posX, posY from cache;
                        if (usePosCache || child.dragging) {
                            if (child.x != null) {
                                child.posX = child.x;
                            }

                            if (child.y != null) {
                                child.posY = child.y;
                            }
                        }

                        if (child.posX == null) {
                            child.posX = startX;
                        }

                        if (child.posY == null) {
                            child.posY = startY + offset + child.size / 2;
                        }

                        offset += child.size;

                        var sX = source.posX;
                        var sY = source.posY;
                        var tX = child.posX;
                        var tY = child.posY;

                        createConnect(child, nodes, links);

                        if (source.id !== rootCluster.id || !usePosCache) {
                            links.push({
                                source: {
                                    x: sX,
                                    y: sY
                                },
                                target: {
                                    x: tX,
                                    y: tY
                                },
                                childName: child.childName
                            });
                        }
                    });
                }

                if (source.id !== rootCluster.id || !usePosCache) {
                    nodes.push(source);
                }
            }

            // for dragging node;
            function cloneDraggingNode(node) {
                dragGroup.selectAll("path.connect").data([]).exit().remove();
                dragGroup.selectAll("g.node").data([]).exit().remove();

                var cloneNodes = [node];
                var clonePaths = [];

                function recursive(source) {
                    if (!source || !source.children) return;
                    source.children.forEach(function(child) {
                        cloneNodes.push(child);
                        clonePaths.push({
                            source: {
                                x: source.posX,
                                y: source.posY
                            },
                            target: {
                                x: child.posX,
                                y: child.posY
                            }
                        })
                        recursive(child);
                    });
                }

                recursive(node);

                var pathGroup = dragGroup.selectAll("path.connect").data(clonePaths).enter();
                pathGroup
                .append("path")
                .attr("class", "connect")
                .attr({
                    'd': function(d) {
                        var path = "";
                        if (!isVertical) {
                            path = [
                                "M", d.source.x, d.source.y,
                                "L", d.target.x, d.target.y
                            ].join(" ");
                        } else {
                            path = [
                                "M", -d.source.y, d.source.x,
                                "L", -d.target.y, d.target.x
                            ].join(" ");
                        }
                        return path;
                    },
                    'fill': 'none',
                    'stroke': '#ccc',
                    'stroke-width': '2px'
                });

                var nodeGroup = dragGroup.selectAll("g.node").data(cloneNodes);
                var nodeEnter = nodeGroup.enter()
                .append("g")
                .attr("class", "node")
                .attr("transform", function(d) {
                    var gPos = "";
                    if (!isVertical) {
                        gPos = "translate(" + d.posX + "," + d.posY + ")";
                    } else {
                        gPos = "translate(" + -d.posY + "," + d.posX + ")";
                    }
                    return gPos;
                });

                nodeEnter
                .append("circle")
                .attr({
                    "r": 6,
                    "stroke": "lightsteelblue",
                    "stroke-width": "1px",
                    "fill": function(d) {
                        var dcolor = "#c6dbef";
                        if (d._children) {
                            dcolor = "#fd8d3c";
                        } else if (d.children && d.children.length) {
                            dcolor = "#3182bd";
                        }
                        return dcolor;
                    }
                });

                nodeEnter
                .append('text')
                .text(function(d) {
                    var nodeName = d.name;
                    return nodeName.substr(0, 12);
                })
                .attr({
                    "text-anchor": "start",
                    "transform": function(d) {
                        var trans = "";
                        if (!isVertical) {
                            trans = "translate(15, 5)";
                        } else {
                            trans = "translate(-5, 15) rotate(90)"
                        }
                        return trans;
                    }
                })
                .style({
                    "font-size": "14px"
                });
            }

            function renderNode() {
                // clear graph;
                pathGroup.selectAll("path.connect").data([]).exit().remove();
                nodeGroup.selectAll("g.node").data([]).exit().remove();

                if (usePosCache) {
                    if (!isVertical) {
                        rootCluster.posX = rootCluster.size / 2;
                    } else {
                        rootCluster.posY = rootCluster.size / 2;
                    }
                    // reset old data;
                    for (var i in sourceNodes) {
                        sourceNodes[i].parentId = null;
                    }
                } else {
                    // reset old data;
                    for (var i in sourceNodes) {
                        sourceNodes[i].posX = null;
                        sourceNodes[i].posY = null;
                        sourceNodes[i].parentId = null;
                    }
                    rootCluster.posX = 0;
                    rootCluster.posY = 0;
                }

                console.log("rootCluster");
                estimateSize(rootCluster);
                console.log("after rootCluster", [].concat(rootCluster.children));
                var nodes = [];
                var links = [];
                createConnect(rootCluster, nodes, links);
                //console.log("rootCluster", rootCluster, nodes, links);
                // store current cluster;
                currentCluster = JSON.stringify(rootCluster);

                var pathView = pathGroup.selectAll("path.connect").data(links);
                var pathEnter = pathView.enter()
                    .append("path")
                    .attr("class", "connect")
                    .attr({
                        'd': function(d) {
                            var path = "";
                            if (!isVertical) {
                                path = [
                                    "M", d.source.x, d.source.y,
                                    "L", d.target.x, d.target.y
                                ].join(" ");
                            } else {
                                path = [
                                    "M", -d.source.y, d.source.x,
                                    "L", -d.target.y, d.target.x
                                ].join(" ");
                            }
                            return path;
                        },
                        'fill': 'none',
                        'stroke': function(d) {
                            var childName = d.childName.join("::").toLowerCase();
                            if (textSearch && childName.indexOf(textSearch) > -1) {
                                return "#fd8d3c";
                            } else {
                                return  "#ccc";
                            }
                        },
                        'stroke-width': '2px'
                    });



                var nodeView = nodeGroup.selectAll("g.node").data(nodes);
                var nodeEnter = nodeView.enter()
                    .append("g")
                    .attr({
                        "class": "node",
                        "size": function(d) {return d.size}
                    })
                    .attr("transform", function(d) {
                        maxX = Math.max(maxX, d.posX);
                        maxY = Math.max(maxY, d.posY);

                        minX = Math.min(minX, d.posX);
                        minY = Math.min(minY, d.posY);

                        var gPos = "";
                        if (!isVertical) {
                            gPos = "translate(" + d.posX + "," + d.posY + ")";
                        } else {
                            gPos = "translate(" + -d.posY + "," + d.posX + ")";
                        }
                        return gPos;
                    });

                    nodeEnter
                    .append("circle")
                    .attr({
                        "id": function(d) {return d.id},
                        "r": 6,
                        "stroke": "lightsteelblue",
                        "stroke-width": "1px",
                        "posX": function(d) {return d.posX},
                        "posY": function(d) {return d.posY},
                        "fill": function(d) {
                            var dcolor = "#c6dbef";
                            if (d._children) {
                                dcolor = "#fd8d3c";
                            } else if (d.children && d.children.length) {
                                dcolor = "#3182bd";
                            }
                            return dcolor;
                        }
                    })
                    .call(dragListener)
                    .on("click", function(d) {
                        //toggle node child;
                        if (!d3.event.defaultPrevented) {
                            if (d.children) {
                                d._children = d.children;
                                d.children = null;
                            } else {
                                d.children = d._children;
                                d._children = null;
                            }
                            renderNode();
                        }
                    });


                    nodeEnter
                    .append('text')
                    .text(function(d) {
                        var nodeName = d.name;
                        return nodeName.substr(0, 12);
                    })
                    .attr({
                        "text-anchor": "start",
                        "transform": function(d) {
                            var trans = "";
                            if (!isVertical) {
                                trans = "translate(15, 5)";
                            } else {
                                trans = "translate(-5, 15) rotate(90)"
                            }
                            return trans;
                        }
                    })
                    .style({
                        "font-size": "14px"
                    });

                // update svg size;
                contentWidth = !isVertical ? (maxX - minX) : (maxY - minY);
                contentHeight = !isVertical ? (maxY - minY) : (maxX - minX);

                baseSvg.attr("width", Math.max(svgWidth, contentWidth));
                baseSvg.attr("height", Math.max(svgHeight, contentHeight));

                $("#scroll-box .scroll-content")
                .width(Math.max(svgWidth, contentWidth))
                .height(Math.max(svgHeight, contentHeight));
            }


            renderNode();
            centerNode(rootCluster);
        },
        showLocation = function(jsonData, textSearch) {
            var activeTab = d3.select(".tab-content .active");
            var svgSize = activeTab.node().getBoundingClientRect();
            var padding = 48;

            var minX = 9999;
            var minY = 9999;
            var maxX = -9999;
            var maxY = -9999;
            var contentWidth = 0;
            var contentHeight = 0;

            var svgWidth = svgSize.width;
            var svgHeight = svgSize.height;
            var showAllTree = showFullTree;
            textSearch = textSearch.toLowerCase();

            function zoom() {
                svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            }

            var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

            var baseSvg = activeTab
                            .html("")
                            .append("svg")
                            .attr("width", "100%")
                            .attr("height", "100%").call(zoomListener);

            var svgGroup = baseSvg
                            .append("g")
                            .attr("class", "pan-cover")
                            .attr("transform", "translate(" + padding + "," + padding + ")");

            var pathGroup = svgGroup
                            .append("g")
                            .attr("class", "group path-group");

            var nodeGroup = svgGroup
                            .append("g")
                            .attr("class", "group node-group");

            var sourceNodes = {}; // node dictionary;
            var zoomLevel = 100;
            var rootCluster = {
                children: [],
                size: 0,
                name: "Root",
                latitude: 0,
                longitude: 0
            };

            jsonData.forEach(function(node) {

                if (!node.name) {
                    node.name = node.friendlyName;
                }

                var newNode = Object.assign({}, node);
                var bigLat = Math.round(newNode.latitude * zoomLevel);
                var bigLon = Math.round(newNode.longitude * zoomLevel);

                // add to dictionary;
                sourceNodes[newNode.id] = newNode;
                newNode.size = 20;

                if (Math.abs(newNode.latitude) && Math.abs(newNode.latitude)) {
                    // latitude = [-90, 90];
                    // longitude = [-180, 180];
                    maxY = Math.max(maxY, newNode.latitude);
                    minY = Math.min(minY, newNode.latitude);

                    maxX = Math.max(maxX, newNode.longitude);
                    minX = Math.min(minX, newNode.longitude);

                    // the first group display;
                    if (!node.parents || !node.parents.length) {
                        var id = bigLat + "/" + bigLon;
                        var lat = bigLat / zoomLevel;
                        var lon = bigLon / zoomLevel;

                        if (sourceNodes[id] == null) {
                            // create new cluster;
                            sourceNodes[id] = {
                                _children: [],
                                size: 0,
                                id: id,
                                children: null,
                                latitude: lat,
                                longitude: lon,
                                isCluster: true
                            };

                            rootCluster.children.push(id);
                            rootCluster.latitude += Number(lat);
                            rootCluster.longitude += Number(lon);
                        }

                        sourceNodes[id]._children.push(newNode.id);
                    }
                }
            });

            // set average latitude/longitude for root cluster;
            if (rootCluster.children.length) {
                rootCluster.longitude = rootCluster.longitude / rootCluster.children.length;
                rootCluster.latitude = rootCluster.latitude / rootCluster.children.length;
            }

            var scaleX = function(longitude) { return (longitude - minX) * 4000; }
            var scaleY = function(latitude) { return (latitude - minY) * 4000; }

            // every node holds all child name of it;
            function getChildName(source) {
                var childName = [source.name];

                (source.children || source._children).forEach(function(item) {
                    var child = sourceNodes[item.toString()] || item;
                    if (child && !child.hasChildName) {
                        // don't allow multi parents;
                        child.hasChildName = true;
                        childName = childName.concat(getChildName(child));
                    }
                });

                source.childName = childName;
                return childName;
            }

            getChildName(rootCluster);

            function estimateSize(source) {
                var children = [];
                var size = 0;

                if (source.children) {
                    source.children.forEach(function(item) {
                        var child = sourceNodes[item.toString()] || item;
                        if (child) {
                            var childName = child.childName.join("::").toLowerCase();
                            if (showAllTree || !textSearch || childName.indexOf(textSearch) > -1) {
                                // allow multi parents;
                                // children.push(child);
                                if (!child.hasParent) {
                                    children.push(child); // don't allow multi parents;
                                    child.hasParent = true;
                                    size += estimateSize(child);
                                }
                            }
                        }
                    });
                    source.children = children;
                }

                source.size = size ? size : 20;
                return source.size;
            }


            function createConnect(source, nodes, links) {
                if (!source) return false;
                var children = source.children;
                var offset = 0;
                var size = source.size;
                var startX = source.posX + 200;
                var startY = source.posY - size / 2;

                if (children && children.length) {

                    children.forEach(function(child) {
                        if (child.posX == null) {
                            // for first group;
                            if (child.isCluster) {
                                child.posX = scaleX(child.longitude);
                            } else {
                                child.posX = startX;
                            }
                        }

                        if (child.posY == null) {
                            // for first group;
                            if (child.isCluster) {
                                child.posY = scaleY(child.latitude);
                            } else {
                                child.posY = startY + offset + child.size / 2;
                            }
                        }

                        offset += child.size;

                        var sX = source.posX;
                        var sY = source.posY;
                        var tX = child.posX;
                        var tY = child.posY;

                        createConnect(child, nodes, links);

                        links.push({
                            source: {
                                x: sX,
                                y: sY
                            },
                            target: {
                                x: tX,
                                y: tY
                            },
                            childName: child.childName
                        });
                    });
                }

                nodes.push(source);
            }



            function renderNode() {
                // clear graph;
                pathGroup.selectAll("path.connect").data([]).exit().remove();
                nodeGroup.selectAll("g.node").data([]).exit().remove();

                // reset old data;
                for (var i in sourceNodes) {
                    sourceNodes[i].posX = null;
                    sourceNodes[i].posY = null;
                    sourceNodes[i].hasParent = null;
                }
                var graphHeight = estimateSize(rootCluster);
                rootCluster.posX = scaleX(rootCluster.longitude);
                rootCluster.posY = scaleY(rootCluster.latitude);
                var links = [];
                var nodes = [];
                createConnect(rootCluster, nodes, links);

                var pathView = pathGroup.selectAll("path.connect").data(links);
                var pathEnter = pathView.enter()
                    .append("path")
                    .attr("class", "connect")
                    .attr({
                        'd': function(d) {
                            return [
                                "M", d.source.x, d.source.y,
                                "L", d.target.x, d.target.y
                            ].join(" ")
                        },
                        //'d': d3.svg.diagonal(), // curve path;
                        'fill': 'none',
                        'stroke': function(d) {
                            var childName = d.childName.join("::").toLowerCase();
                            if (textSearch && childName.indexOf(textSearch) > -1) {
                                return "#fd8d3c";
                            } else {
                                return  "#ccc";
                            }
                        },
                        'stroke-width': '2px'
                    });


                var nodeView = nodeGroup.selectAll("g.node").data(nodes);
                var nodeEnter = nodeView.enter()
                    .append("g")
                    .attr("class", "node")
                    .attr("transform", function(d) {
                        maxX = Math.max(maxX, d.posX);
                        maxY = Math.max(maxY, d.posY);

                        minX = Math.min(minX, d.posX);
                        minY = Math.min(minY, d.posY);

                        return "translate(" + d.posX + "," + d.posY + ")";
                    });

                    nodeEnter
                    .append("circle")
                    .attr({
                        "id": function(d) {return d.id},
                        "r": 6,
                        "stroke": "lightsteelblue",
                        "stroke-width": "1px",
                        "fill": function(d) {
                            var dcolor = "#c6dbef";
                            if (d._children) {
                                dcolor = "#fd8d3c";
                            } else if (d.children && d.children.length) {
                                dcolor = "#3182bd";
                            }
                            return dcolor;
                        }
                    })
                    .on("click", function(d) {
                        //toggle node child;
                        if (!d3.event.defaultPrevented) {
                            if (d.children) {
                                d._children = d.children;
                                d.children = null;
                            } else {
                                d.children = d._children;
                                d._children = null;
                            }
                            renderNode();
                        }
                    });

                    nodeEnter
                    .append('text')
                    .text(function(d) {
                        return d.name;
                    })
                    .attr({
                        "y": "20px",
                        "text-anchor": "middle"
                    })
                    .style({
                        "font-size": "14px"
                    });

                // update svg size;
                contentWidth = !isVertical ? (maxX - minX) : (maxY - minY);
                contentHeight = !isVertical ? (maxY - minY) : (maxX - minX);

                baseSvg.attr("width", Math.max(svgWidth, contentWidth));
                baseSvg.attr("height", Math.max(svgHeight, contentHeight));

                $("#scroll-box .scroll-content")
                .width(Math.max(svgWidth, contentWidth))
                .height(Math.max(svgHeight, contentHeight));
            }

            renderNode();
        },
        showGraph = function(tab, jsonData, textSearch) {
            if (!tab) return;
            switch (tab) {
                case 'network':
                    showNetwork();
                    break;
                case 'showdnd':
                    showDND();
                    break;
                case 'tree':
                    drawTree(jsonData, textSearch);
                    break;
                case 'radial':
                    showRadial();
                    break;
                case 'location':
                    showLocation(jsonData, textSearch);
                    break;
            }
        },
        init = function() {

            $loading = $('#loading');
            $results = $('#results');

            var $tooltip = $('#tooltip'),
                jsonData,
                currentTab = 'tree';

            flatOutputFile = document.getElementById('flat-output');
            jsonOutputFile = document.getElementById('json-output');
            $uploadButton = $('#upload');

            symbol = d3.svg.symbol().size(SHAPE_SIZE).type;
            shape = {
                'A' : symbol('diamond')(),
                'B' : symbol('circle')(),
                'C' : symbol('triangle-up')(),
                'G' : symbol('square')(),
                'R' : symbol('cross')(),
                DEFAULT: symbol('triangle-down')()
            };

            setUpload();
            $('#search-button').on('click', function() {
                var textSearch = $('#search-input').val();
                showGraph(currentTab, jsonData, textSearch);
                return false;
            });

            $('#reset-search').on('click', function() {
                if (jsonData.length || jsonData.name) {
                    showGraph(currentTab, jsonData, "");
                }
                return false;
            });

            $("#rotate-button").click(function() {
                var textSearch = $('#search-input').val();
                isVertical = !!!isVertical;
                if (jsonData.length || jsonData.name) {
                    showGraph(currentTab, jsonData, textSearch);
                }
            });

            $("#use-cache-pos").on('click', function() {
                usePosCache = !!!usePosCache;
                isVertical = false; // always set graph horizonal;
                if (jsonData.name || jsonData.length) {
                    showGraph(currentTab, jsonData, "");
                }

                if (usePosCache) {
                    $(this).html("Cache: On");
                } else {
                    $(this).html("Cache: Off");
                }
                return false;
            });

            $("#save-to-store").on('click', function() {
                if (currentData && currentCluster) {
                    localStorage.setItem(currentData, currentCluster);
                }
            });

            $("#clear-local-store").on('click', function() {
                localStorage.clear();
                window.location.reload();
            });

            // radio button change;
            $('#show-full-tree input[type=radio]').on('change', function() {
                var textSearch = $('#search-input').val();
                showFullTree = parseInt($(this).val());
                if (textSearch && jsonData.length) {
                    showGraph(currentTab, jsonData, textSearch);
                }
                return false;
            });

            $('#tooltip').on('click', function() {
                hideToolTip();
            });

            //https://jsfiddle.net/b8wsxr89/
            $('#filter').multiselect({
                includeSelectAllOption: true,
                nonSelectedText: 'Filter'
            });

            $('#select-data li a').on('click', function() {

                var this_html = this.innerHTML;
                if (currentData !== this_html) {
                    currentData = this_html;
                    searchInput = '';
                    $('#data-selected').text(this_html);
                    this_html = this_html.toLowerCase();
                    rootName = this_html.toUpperCase();
                    // try to get data from localStorage;
                    var storedData = localStorage.getItem(currentData);
                    if (storedData) {
                        jsonData = JSON.parse(storedData);
                        isVertical = false;
                        usePosCache = true;
                        showGraph(currentTab, jsonData, "");
                    } else {
                        // get data via ajax;
                        var file = 'data/' + this_html + '.json?id=' + Date.now();
                        usePosCache = false;
                        isVertical = true;
                        hideToolTip();
                        $.ajax({
                            type: 'GET',
                            url: file,
                            dataType: 'json',
                            success: function(json) {
                                if (json instanceof Array) {
                                    onlyJSON = true;
                                    flatTreeArray = json;
                                    origTree = false;
                                } else {
                                    onlyJSON = false;
                                    origTree = json;
                                }
                                jsonData = json;
                                showGraph(currentTab, json, "");
                            },
                            error: function(e1, e2, e3, e4) {}
                        })
                    }

                    if (usePosCache) {
                        $("#use-cache-pos").html("Cache: On");
                    } else {
                        $("#use-cache-pos").html("Cache: Off");
                    }
                }
            });


            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                var target = $(e.target).attr("href");
                var regex = /\#(.*?)\-/;
                var tab = regex.exec(target)[1];
                if (currentTab != tab) {
                    currentTab = tab;
                    if (jsonData) {
                        showGraph(currentTab, jsonData, "");
                    }
                }
            });

            // show/hide virtual scroll area;
            $(document.body).keydown(function(e) {
                if (e.ctrlKey) {
                    $('#scroll-box').show();
                }
            }).keyup(function(e) {
                $('#scroll-box').hide();
            });

            $('#scroll-box').scroll(function(e) {
                $(".tab-pane.in").scrollTop(this.scrollTop).scrollLeft(this.scrollLeft);
            });
        };

    $(document).ready(function() {
        init();
    })
})();
